# Lua-stdfs

Lua filesystem API library.
It is basically a wrapper for C++ std::filesystem.

Assumes all input path strings are UTF-8. If your Lua script has path string defined directly in code, make sure the script file is saved with UTF-8 encoding. If you get the string elsewhere, you're responsible to convert it to UTF-8.

# API

### path(p)

Constructs a `path` user data. Parameter can be a string or another `path` object.

Available `path` operators:

  - `..` concatenates paths as if they were strings
  - `/` concatenates paths using system-dependent path separator
  - `==` checks whether 2 paths resolve to the same file system entity
  - `tostring` converts a `path` object to string

The following list of properties is available:

  - `absolute` composes absolute path from this
  - `canonical` composes canonical path from this
  - `is_absolute` checks if the path is absolute
  - `is_relative` checks if the path is not absolute
  - `lexically_normal` composes normalized path from this
  - `lexically_proximate(base)` returns this path proximate to base path + normalized (proximate means if this.lexically_relative(base) is not empty, return that, otherwise - this)
  - `lexically_relative(base)` returns this path relative to base path + normalized
  - `ext` returns path's extension for files, empty path for directories
  - `filename` returns path's file name with extension
  - `parent` returns parent directory of the path
  - `stem` returns the filename without extension
  - `repl_filename(newp)` replaces the last path component with another path
  - `repl_ext(newext)` replaces the extension

### copy(src, dst [, (copy-opts | old-opt))

Copies files and directories.

- if `src` is a regular file
  - if `dst` is a directory, copies the file into that directory
  - otherwise does usual file copying
- if `src` is a directory
  - if `dst` doesn't exist, first creates it
  - then iterates contents of `src` and copies all the files on the 1st level into `dst` (may be subject to change later to provide recursive copying)
- 3rd argument is an options argument given in either one of the following forms
- `copy-opts` is an enum value, allowing OR (|), AND (&) and XOR (~) operations for values
  - available options are provided in module global table `copy_opts`. Meaning for all options are as described in C++ reference at [cppreference - copy_options](https://en.cppreference.com/w/cpp/filesystem/copy_options)
    - `stdfs.copy_opts.none`
    - `stdfs.copy_opts.skip_existing`
    - `stdfs.copy_opts.overwrite_existing`
    - `stdfs.copy_opts.update_existing`
    - `stdfs.copy_opts.recursive`
    - `stdfs.copy_opts.copy_symlinks`
    - `stdfs.copy_opts.skip_symlinks`
    - `stdfs.copy_opts.directories_only`
    - `stdfs.copy_opts.create_symlinks`
    - `stdfs.copy_opts.create_hard_links`
- `old-opt` is a table of options in key-value form (deprecated)
  - `existing` allows controlling what happens if when destination files exist
    - `o` for overwrite: always overwrites destination
    - `s` for skip: always skips if destination exists
    - `u` for update: overwrites destination only if it's older
Examples:
`stdfs.copy('src', 'dst', stdfs.copy_opts.skip_existing | stdfs.copy_opts.recursive)`
`stdfs.copy('src', 'dst', { existing = "o" })`

### create_dir(p)

Creates directory tree, meaning also creates parent directories for the path if they don't exist.

### create_hardlink(tgt, src)

Creates a hardlink `src` that points to `tgt` path.
Some operating systems and file systems do not support hard links.

### create_symlink(tgt, src [, is_directory])

Creates a symlink `src` that points to `tgt` path. Creating file and directory symlinks may differ on some platforms/file systems, that's why there is an optional boolean hint 3rd parameter.
If target exists, code will try to guess if it's a directory and use the appropriate mechanism.
Some operating systems and file systems do not support symbolic links.

### exists(p)

Check if path resolves to an existing file system entity.
Note that on some systems this might fail for symlink files. Use `fs.symlink_status()` to check those.

### equiv(lhp, rhp)

Same as `lhp == rhp`

### file_size(p)

For regular files returns file size in bytes. Behavior is not defined for directories.

### file_status(p)

Returns a table containing target path's attributes. Contains target file type and permissions. Follows symlink to its target.
The type can be checked against `stdfs.file_type`:

```lua
stdfs.file_type = {
	none,
	not_found,
	regular,
	directory,
	symlink,
	block,
	character,
	fifo,
	socket,
	unknown
}
```

For `stdfs.file_perms` values, see `permissions()` function below.

Example usage (check examples folder for more).
```lua
fs = require("stdfs")
fstat = fs.file_status(p)
if fstat.file_type == fs.file_type.regular then ... end
```

### hardlink_count(tgt)

Queries number of filesystem hard links to target path.
Some operating systems and file systems do not support hard links.

### ls(p, recursive)

Creates an iterator through directory `p`. Second parameter is an optional boolean indicating recursive iteration.

Skips any directories/files that raise permission denied on access.

Usage is as follows: `for path in stdfs.ls("/home/user") do ... end`

### permissions(p)

Changes target path permissions if possible.

Usage:
```lua
stdfs.permissions(<target_path>, <file_perms>[, <permission_opts>) 
```
- `file_perms` is a logical expression comprised of values from `stdfs.file_perms`.
- `permission_opts` is an optional parameter from `stdfs.perm_options`.
Example:
```lua
-- adds executable flag for owner and group to the file "target.sh"
stdfs.permission("target.sh", stdfs.file_perms.owner_exec | stdfs.file_perms.owner_exec, stdfs.perm_options.add)
-- set all permissions for owner clearing all else
stdfs.permission("target.sh", stdfs.file_perms.owner_all, stdfs.perm_options.replace)
-- remove write permissions for anyone but owner
stdfs.permission("target.sh", stdfs.file_perms.group_write | stdfs.file_perms.others_write, stdfs.perm_options.remove)
```
All available values for the parameters:
```lua
stdfs.file_perms = {
	none,
	owner_read,
	owner_write,
	owner_exec,
	owner_all,
	group_read,
	group_write,
	group_exec,
	group_all,
	others_read,
	others_write,
	others_exec,
	others_all,
	all,
	set_uid,
	set_gid,
	sticky_bit,
	mask,
	unknown
}
stdfs.perm_options = {
	replace,  -- replace all permissions with provided value
	add,      -- add provided premissions to existing
	remove,   -- remove provided permissions
	nofollow  -- do not follow symlinks (apply to symlink itself)
}
```

### proximate(path, base)

Returns `path` made proximate to `base`. Same as `relative(path, base)` except `proximate` would return `path` when there's no obvious way to make relative.

### read_symlink(p)

Queries target path of a symlink. Considered an error if `p` is not a symlink.

### relative(path, base)

Returns `path` made relative to `base`.

### remove(p)

Removes a file or empty directory.
Returns boolean indicating if target was deleted.

### remove_all(p)

Removes a file or directory recursively.
Returns number of removed filesystem objects.

### rename(src, dst)

Renames (moves) `src` to `dst`. Can't replace non-empty directories.

### resize(tgt, new_size)

Changes the size of the regular file. If new size is smaller, tail is truncated. Is new size is larger, it's zero-padded.

### space(tgt)

Determines available and free space on the filesystem where `tgt` is located.
Returns a table with `capacity`, `free` and `available` fields. Namely, total size in bytes, free space in bytes and available to non-privileged process in bytes.

### symlink_status(p)

Same as `file_status(p)`, but if `p` is a symlink, it's not followed, attributes are its own, not its target.

### temp_dir()

Returns a system directory location suitable for temporary files.

### work_dir(p)

Returns (if `p` is nil) or changes current working directory.

### write_time(p)

Returns a table with last write time info of the target file or directory by Gregorian calendar GMT+0 and local timezones.
Table has the following fields:

  - `sec` - seconds [0, 60]
  - `min` - minutes [0, 59]
  - `hour` - hours [0, 23]
  - `mday` - month day [1, 31]
  - `mon` - month [1, 12]
  - `year` - year
  - `wday` - week day starting at Monday [1, 7]
  - `yday` - day of year since January 1st [1, 365]
  - `isdst` - Daylight Saving Time flag. True if enabled, false otherwise
  - `since_epoch` - seconds since epoch. Might differ on different platforms
  - `ctm` - a string representation of this date-time as given by C++ `std::ctime`
  - `loc` - a table with same set of fields (except `isdst` and `loc`) but in local time zone


# TODO
  - let CMake find doctest
  - write a note about Lua 5.1, 5.2 and bitwise operations
    - put test directory structure creation into Lua script