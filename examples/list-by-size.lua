fs = require('stdfs')

if #arg < 3 then
    print('Arguments: 1. path to target directory 2. lower size bound 3. upper size bound')
end

local targetDir = arg[1]
local targetDir = fs.path(arg[1])
if not fs.exists(targetDir) then
    print("Target directory doesn't exist: " .. tostring(targetDir))
    return
end

local sizeLower = tonumber(arg[2])
local sizeUpper = tonumber(arg[3])
if sizeLower == nil or sizeUpper == nil then
    print("Error parsing lower or upper size")
    return
end

for p in fs.ls(targetDir, true) do
    local stat = fs.file_status(p)
    if stat.file_type == fs.file_type.regular then
        local fsize = fs.file_size(p)
        if fsize >= sizeLower and fsize <= sizeUpper then
            print(tostring(fsize) .. " bytes: " .. tostring(p))
        end
    end
end
