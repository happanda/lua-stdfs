fs = require('stdfs')

if #arg < 1 then
    print('Arguments: 1. path to target')
end

local target = arg[1]
local fstat = fs.file_status(target)
if fstat.file_type == fs.file_type.regular then
    if 0 ~= (fstat.file_perms & fs.file_perms.owner_exec) then
        print("Is executable by owner")
    else
        print("Not executable by owner")
    end
else
    print("Not a regular file")
end