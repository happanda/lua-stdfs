set_xmakever("2.8.6") -- probably lower, something like 2.7.3

-- used to load modules when calling 'import'
add_moduledirs("$(projectdir)/ext")

if is_host("windows") then
    if "$(env LUA_DIR)" == "" then
        if "$(env LUA_PATH)" ~= "" then
            set_runenv("LUA_DIR", "$(env LUA_PATH)")
        else
            xmake.error("Define LUA_DIR to point to Lua directory!")
        end
    end
end

-- stdfs version
set_version("0.8.2")

add_requires("pkg-config", {system = true})
add_requires("lua", { system = true })
add_requires("doctest 2.4.8")

-- should also add for CXX,ARMClang,AppleClang,LCC?
add_cxxflags("gcc::-std=c++17")
add_cxxflags("gcc::-Wall -Wextra -Wformat=2 -Wunused")
add_cxxflags("clang::-std=c++17")
add_cxxflags("clang::-Wall -Wextra -Wshadow -Wformat=2 -Wunused")
add_cxxflags("msvc::-std:c++17")
add_cxxflags("msvc::-W3 -O2 -Zc:__cplusplus")

if is_mode("debug") then
    add_defines("DEBUG")
    set_config("buildir", "build/debug")
else
    set_config("buildir", "build/release")
end
set_targetdir("$(buildir)")

local testsdir = "$(projectdir)/tests"

-- configuration files and where they will be placed
-- build directory by default, but can set with set_configdir("$(buildir)")
add_configfiles("version.h.in", { pattern = "@(.-)@" })

-- main hekate library
target("stdfs")
    set_kind("shared")
    --set_prefixname("")

    -- source files
    add_files("src/*.cpp")
    add_includedirs("src", "$(buildir)")
    --remove_files("src/??.cpp")

    -- link to
    add_packages("lua", { public = true })

    after_build(function (target)
        os.run("cp %s " .. testsdir, target:targetfile())
    end)

-- test app
target("stdfs-test")
    set_kind("binary")

    add_packages("doctest")
    add_packages("lua")

    add_deps("prepare")
    add_deps("stdfs")

    add_files("src/test/*.cpp")
    remove_files("src/test/prepare.cpp")

    add_includedirs("src", "src/test", "$(buildir)")
    --add_links("stdc++fs", { tools = "gcc" })

    after_build(function (target)
        os.run("cp %s " .. testsdir, target:targetfile())
    end)

target("prepare")
    set_kind("binary")

    add_files("src/test/prepare.cpp", "src/test/utils.cpp")
    add_includedirs("src/test")

    after_build(function (target)
        os.run("cp %s " .. testsdir, target:targetfile())
    end)
