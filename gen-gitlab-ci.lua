-- quick hack
package.path = "./docker/?.lua;" .. package.path
config = require("images-config")

local yml_template = {
stages = [[stages:
  - build_and_test

variables:
]],

steps = [[

.build-steps: &build_steps
  - whoami
  - cp -r $CI_PROJECT_DIR ~
  - cd ~/lua-stdfs
  - ls -l
  - cmake -B build -DDOCTEST_REPO_DIR:PATH=/home/developer/doctest
  - make -C build

.test-steps: &test_steps
  - whoami
  - cd ~/lua-stdfs/tests
  - ./stdfs_test

]]
}


local keys_sorted = {}
for k, _ in pairs(config) do
    table.insert(keys_sorted, k)
end

table.sort(keys_sorted)

io.write(yml_template["stages"])

-- UBU_IMAGE_1: "c0d3castl3/lua-stdfs:ubuntu-latest-lua5.1"
local var_names = {}
local step_names = {}

for _, distro in ipairs(keys_sorted) do
    local var_pre = "IMG_" .. distro .. "_"
    for v, version in ipairs(config[distro]) do
        for i, luaver in ipairs(version["lua_versions"]) do
            local var_name = var_pre .. tostring(i)
            local var_str = var_name .. ": c0d3castl3/lua-stdfs:"
                .. distro .. "-" .. version["version"] .. "-lua" .. luaver
            io.write("  " .. var_str .. "\n")

            table.insert(var_names, var_name)
            table.insert(step_names, distro .. "-" .. version["version"] .. "-lua" .. luaver)
        end
    end
end

io.write(yml_template["steps"])

for i, step in ipairs(step_names) do
    io.write("bt " .. step .. ":\n")
    io.write("  stage: build_and_test\n")
    io.write("  image: $" .. var_names[i] .. "\n")
    io.write("  script:\n")
    io.write("    - *build_steps\n")
    io.write("    - *test_steps\n")
end
