fs = require("stdfs")
config = require("images-config")

local out_dir = fs.path("gen")


local eprint = function(msg)
    io.stderr:write("[Error] " .. msg .. "\n")
end

local build_image = function(image_name, docker_file, docker_str)
    if fs.exists(docker_file) then
        local docker_on_disk = ""
        for line in io.lines(docker_file) do
            docker_on_disk = docker_on_disk .. line .. "\n"
        end
        if docker_on_disk ~= docker_str then
            eprint("File exists and differs: " .. docker_file)
            return false
        end
    end
    
    docker_file = out_dir / docker_file
    local image_file = (out_dir / image_name) .. ".log"

    local fw = io.open(tostring(docker_file), "w")
    fw:write(docker_str)
    fw:close()

    print("Written " .. tostring(docker_file))
    local command = "DOCKER_BUILDKIT=1 docker build --progress=plain -t "
        .. image_name .. " -f " .. tostring(docker_file)
        .. " . &> " .. tostring(image_file)
    print("Exec: " .. command)
    local res, reason, error = os.execute(command)
    if res ~= true then
        eprint("docker build failed. Finished by '" .. reason .. "' with error " .. tostring(error))
        return false
    end

    command = "docker image tag " .. image_name .. " c0d3castl3/" .. image_name
    print("Exec: " .. command)
    local res, reason, error = os.execute(command)
    if res ~= true then
        eprint("docker image tag failed. Finished by '" .. reason .. "' with error " .. tostring(error))
        return false
    end
    return true
end

local chosen_config = nil
if arg[1] ~= nil then
    chosen_config = arg[1]
end

for tag, cfg in pairs(config) do
    if chosen_config ~= nil and tag ~= chosen_config then
        goto continue_cfg
    end
    print("Images for " .. tag)

    local templ_filename = tag .. "-template.docker"
    if not fs.exists(templ_filename) then
        eprint("Template not found: " .. templ_filename)
        goto continue_cfg
    end

    local docker_template = ""
    for line in io.lines(templ_filename) do
        docker_template = docker_template .. line .. "\n"
    end

    for _, entry in ipairs(cfg) do
        local linver = entry.version

        local image_name = "lua-stdfs:" .. tag .. "-" .. linver
        local docker_file = image_name .. ".Dockerfile"

        local docker_str = docker_template
        docker_str = docker_str:gsub("$LINUX_VERSION", linver)

        build_image(image_name, docker_file, docker_str)

        for i, luaver in ipairs(entry.lua_versions) do
            local templ_filename = tag .. "-lua-template.docker"
            if not fs.exists(templ_filename) then
                eprint("Template not found: " .. templ_filename)
                goto continue_luaver
            end

            local docker_template = ""
            for line in io.lines(templ_filename) do
                docker_template = docker_template .. line .. "\n"
            end

            local luabit32 = entry.luabit32[i]
            local docker_str = docker_template
            docker_str = docker_str:gsub("$LINUX_VERSION", linver)
            docker_str = docker_str:gsub("$LUA_VERSION", luaver)
            docker_str = docker_str:gsub("$LUA_BIT32", luabit32)
            
            local image_name = "lua-stdfs:" .. tag .. "-" .. linver .. "-lua" .. luaver
            local docker_file = image_name .. ".Dockerfile"

            build_image(image_name, docker_file, docker_str)

            ::continue_luaver::
        end
    end

    ::continue_cfg::
end


local res, reason, error = os.execute("docker container prune -f && docker image prune -f ")
if res ~= true then
    eprint("docker prune failed. Finished by '" .. reason .. "' with error " .. tostring(error))
end