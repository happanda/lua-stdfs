#/usr/bin/zsh
docker image list --filter=reference="c0d3castl3/lua-stdfs:ubuntu-latest-*" --format '{{.Repository}}:{{.Tag}}' | xargs -L 1 docker push
docker image list --filter=reference="c0d3castl3/lua-stdfs:debian-latest-*" --format '{{.Repository}}:{{.Tag}}' | xargs -L 1 docker push
docker image list --filter=reference="c0d3castl3/lua-stdfs:voidlinux-latest-*" --format '{{.Repository}}:{{.Tag}}' | xargs -L 1 docker push