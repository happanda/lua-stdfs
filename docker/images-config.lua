return {
    ubuntu = {
        {
            version = "latest",
            lua_versions = { "5.1", "5.2", "5.3", "5.4" },
            luabit32 = { "lua-bit32", "lua-bit32", "", "" }
        },
    },
    debian = {
        {
            version = "latest",
            lua_versions = { "5.1", "5.2", "5.3", "5.4" },
            luabit32 = { "lua-bit32", "lua-bit32", "", "" }
        },
    },
    voidlinux = {
        {
            version = "latest",
            lua_versions = { "51", "52", "53" },
            luabit32 = { "lua51-bitlib", "", "" }
        },
    },
}