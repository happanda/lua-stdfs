#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Pushes on stack a path to system temporary folder
     *
     * @return Value returned from \p push_path()
     * @return Result of luaL_error() in case of error
     *
     * Details of the operation can be found at https://en.cppreference.com/w/cpp/filesystem/temp_directory_path
     */
    int temp_dir(lua_State* L);
} // namespace stdfs
