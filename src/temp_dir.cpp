#include "common.h"
#include "temp_dir.h"

namespace fs = std::filesystem;


int stdfs::temp_dir(lua_State* L)
{
    std::error_code ec;
    auto temp_dir = fs::temp_directory_path(ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return push_path(L, temp_dir);
}
