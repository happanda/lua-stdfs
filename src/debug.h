#pragma once


namespace stdfs
{
	[[maybe_unused]] int debug_print(char const* format, ...);
} // namespace stdfs
