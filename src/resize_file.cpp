#include "common.h"
#include "resize_file.h"


namespace fs = std::filesystem;


int stdfs::resize_file(lua_State *L)
{
    fs::path path;
    get_path(L, path, 1, 1);

    if (lua_gettop(L) <= 1)
    {
        return luaL_error(L, "resize_file: requires size in bytes as 2nd argument");
    }

    if (stdfs::lua::isinteger(L, 2) != 1)
    {
        return luaL_error(L, "resize_file: new file size must be an integer");
    }
    lua_Integer const sizeParam = lua_tointeger(L, 2);
    if (sizeParam < 0)
    {
        return luaL_error(L, "resize_file: new file size must be a non-negative number");
    }
    auto const size = static_cast<std::uintmax_t>(sizeParam);

    std::error_code ec;
    fs::resize_file(path, size, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    lua_pushinteger(L, size);
    return 1;
}
