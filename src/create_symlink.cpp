#include "common.h"
#include "create_symlink.h"
#include "lua_wrappers.h"

namespace fs = std::filesystem;


int stdfs::create_symlink(lua_State* L)
{
    fs::path target_path, link_path;
    get_path(L, target_path, 1, 1);
    get_path(L, link_path, 2, 2);

    std::error_code ec;
    bool is_directory = false;
    if (lua_gettop(L) > 2 && lua_isboolean(L, 3))
    {
        // first check if user forces directory/non-directory
        is_directory = lua::tobool(L, 3);
    }
    else
    {
        // then try to guess from target
        is_directory = fs::exists(target_path) && fs::is_directory(target_path, ec);
        // ignore errors here
        ec.clear();
    }

    if (is_directory)
    {
        fs::create_directory_symlink(target_path, link_path, ec);
    }
    else
    {
        fs::create_symlink(target_path, link_path, ec);
    }
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return 0;
}
