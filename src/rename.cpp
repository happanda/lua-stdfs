#include "common.h"
#include "rename.h"

namespace fs = std::filesystem;


int stdfs::rename(lua_State* L)
{
    fs::path old_path, new_path;
    get_path(L, old_path, 1, -2);
    get_path(L, new_path, 2, -1);

    std::error_code ec;
    fs::rename(old_path, new_path, ec);

    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return 0;
}
