#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Pushes on stack number of filesystem hard links to target path
     * @return \a 1 as one integer is pushed on stack in case of success
     * @return Result of \a luaL_error() in case of error
     */
    int hardlink_count(lua_State* L);
} // namespace stdfs
