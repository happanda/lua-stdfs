#pragma once

struct lua_State;

namespace stdfs {
    /**
     * @brief Checks or sets current working directory
     * @param L Lua interpreter state
     * @return 0 if changing directory
     * @return Result of push_path() if querying current directory
     * @return Result of luaL_error() in case of error
     */
    int work_dir(lua_State *L);
} // namespace stdfs
