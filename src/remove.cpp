#include "common.h"
#include "remove.h"
#include "lua_wrappers.h"

namespace fs = std::filesystem;


int stdfs::remove(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const removed = fs::remove(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    lua::pushbool(L, removed);
    return 1;
}

int stdfs::remove_all(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    uintmax_t const removed = fs::remove_all(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    // TODO: static_cast may be incorrect
    lua_pushinteger(L, static_cast<lua_Integer>(removed));
    return 1;
}
