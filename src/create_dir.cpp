#include "common.h"
#include "create_dir.h"
#include "lua_wrappers.h"

namespace fs = std::filesystem;


int stdfs::create_dir(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool created = fs::create_directories(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    lua::pushbool(L, created);
    return 1;
}
