#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Queries available space on file system
     * @return A table with fields <em> capacity, free, available </em>
     * @return Result of luaL_error() in case of error
     *
     * Details of the operation can be found at https://en.cppreference.com/w/cpp/filesystem/resize_file
     */
    int space(lua_State* L);
} // namespace stdfs
