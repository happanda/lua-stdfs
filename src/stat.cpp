#include <chrono>
#include "common.h"
#include "stat.h"

namespace fs = std::filesystem;


int stdfs::write_time(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    fs::file_time_type const write_time = fs::last_write_time(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }

    // TODO: this is not a very precise way to convert between clocks, can do better with C++20
    auto write_time_sys = write_time - fs::file_time_type::clock::now() + std::chrono::system_clock::now();

    std::time_t const timet = std::chrono::system_clock::to_time_t(write_time_sys);
    std::tm const* tm = std::gmtime(&timet);
    char* ctm = std::ctime(&timet);

    lua_newtable(L);
    lua_pushinteger(L, tm->tm_sec);
    lua_setfield(L, -2, "sec");
    lua_pushinteger(L, tm->tm_min);
    lua_setfield(L, -2, "min");
    lua_pushinteger(L, tm->tm_hour);
    lua_setfield(L, -2, "hour");
    lua_pushinteger(L, tm->tm_mday);
    lua_setfield(L, -2, "mday");
    lua_pushinteger(L, tm->tm_mon + 1);
    lua_setfield(L, -2, "mon");
    lua_pushinteger(L, tm->tm_year + 1900);
    lua_setfield(L, -2, "year");
    lua_pushinteger(L, tm->tm_wday);
    lua_setfield(L, -2, "wday");
    lua_pushinteger(L, tm->tm_yday + 1);
    lua_setfield(L, -2, "yday");
    lua_pushinteger(L, tm->tm_isdst);
    lua_setfield(L, -2, "isdst");
    lua_pushinteger(L, timet);
    lua_setfield(L, -2, "since_epoch");
    lua_pushstring(L, ctm);
    lua_setfield(L, -2, "ctm");

    std::tm const* tm_loc = std::localtime(&timet);

    lua_newtable(L);
    lua_pushinteger(L, tm_loc->tm_sec);
    lua_setfield(L, -2, "sec");
    lua_pushinteger(L, tm_loc->tm_min);
    lua_setfield(L, -2, "min");
    lua_pushinteger(L, tm_loc->tm_hour);
    lua_setfield(L, -2, "hour");
    lua_pushinteger(L, tm_loc->tm_mday);
    lua_setfield(L, -2, "mday");
    lua_pushinteger(L, tm_loc->tm_mon + 1);
    lua_setfield(L, -2, "mon");
    lua_pushinteger(L, tm_loc->tm_year + 1900);
    lua_setfield(L, -2, "year");
    lua_pushinteger(L, tm_loc->tm_wday);
    lua_setfield(L, -2, "wday");
    lua_pushinteger(L, tm_loc->tm_yday + 1);
    lua_setfield(L, -2, "yday");
    lua_pushinteger(L, timet);
    lua_setfield(L, -2, "since_epoch");

    lua_setfield(L, -2, "loc");

    return 1;
}
