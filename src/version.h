#pragma once

#include <cstddef>

namespace stdfs {
	std::size_t constexpr VERSION_MAJOR {  };
	std::size_t constexpr VERSION_MINOR {  };
	std::size_t constexpr VERSION_PATCH {  };

	char const* const VERSION_STRING { ".-" };
}
