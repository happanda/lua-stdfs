#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Changes the size of a regular file
     * @return New size of the file in case of success
     * @return Result of luaL_error() in case of error
     *
     * Details of the operation can be found at https://en.cppreference.com/w/cpp/filesystem/resize_file
     */
    int resize_file(lua_State* L);
} // namespace stdfs
