#include "common.h"
#include "copy.h"
#include "file_size.h"
#include <lua.h>

namespace fs = std::filesystem;


int stdfs::copy(lua_State* L)
{
    fs::path old_path, new_path;
    get_path(L, old_path, 1, 1);
    get_path(L, new_path, 2, 2);

    fs::copy_options copyOpts = fs::copy_options::none;
    if (lua_gettop(L) > 2)
    {
        if (stdfs::lua::isinteger(L, 3))
        {
            lua_Integer copyOptsInt = lua_tointeger(L, 3);
            copyOpts = (fs::copy_options)copyOptsInt;
        }
        else if (lua_istable(L, 3))
        {
            lua_pushstring(L, "existing");
            if (LUA_TSTRING == stdfs::lua::gettable(L, 3))
            {
                char const* existingOpt = luaL_checkstring(L, -1);
                if (existingOpt[0] == 'o')
                {
                    copyOpts |= fs::copy_options::overwrite_existing;
                }
                else if (existingOpt[0] == 's')
                {
                    copyOpts |= fs::copy_options::skip_existing;
                }
                else if (existingOpt[0] == 'u')
                {
                    copyOpts |= fs::copy_options::update_existing;
                }
            }
        }
    }

    std::error_code ec;
    fs::copy(old_path, new_path, copyOpts, ec);

    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return 0;
}

int stdfs::create_copy_opts_table(lua_State* L)
{
    std::pair<fs::copy_options, char const*> map[] =
    {
        { fs::copy_options::none,               "none" },
        { fs::copy_options::skip_existing,      "skip_existing" },
        { fs::copy_options::overwrite_existing, "overwrite_existing" },
        { fs::copy_options::update_existing,    "update_existing" },
        { fs::copy_options::recursive,          "recursive" },
        { fs::copy_options::copy_symlinks,      "copy_symlinks" },
        { fs::copy_options::skip_symlinks,      "skip_symlinks" },
        { fs::copy_options::directories_only,   "directories_only" },
        { fs::copy_options::create_symlinks,    "create_symlinks" },
        { fs::copy_options::create_hard_links,  "create_hard_links" },
    };

    lua_newtable(L);
    for (auto const& p : map)
    {
        int const intVal = static_cast<int>(p.first);
        lua_pushinteger(L, intVal);
        lua_setfield(L, -2, p.second);
    }
    return 1;
}
