#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Queries target size
     * @return \a 1 in case of successful query, as one int value is pushed on stack - target size
     * @return Result of \a luaL_error() in case of error
     */
    int file_size(lua_State* L);
} // namespace stdfs
