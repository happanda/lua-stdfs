#include "common.h"

#include "encoding.h"
#include "path.h"
#include "path_arg.h"
#include "lua_wrappers.h"


namespace fs = std::filesystem;
using namespace stdfs;

char const* s_str_meta_path = "stdfs.meta_path";

int stdfs::push_path(lua_State* L, fs::path const& from_path)
{
    void* path_udata = lua_newuserdata(L, sizeof(fs::path));
    new(path_udata) fs::path(from_path);
    luaL_getmetatable(L, s_str_meta_path);
    lua_setmetatable(L, -2);
    return 1;
}

int stdfs::get_path(lua_State* L, std::filesystem::path& out_path, int arg_num/* = 1*/, int index/* = -1*/)
{
    try
    {
        switch (lua_type(L, index))
        {
            case LUA_TSTRING:
            {
                char const* directory = luaL_checkstring(L, index);
                auto wdirectory = stdfs::convert_utf8_utf16(directory);
                if (wdirectory.index() > 0)
                {
                    auto re = std::get<std::range_error>(wdirectory);
                    return luaL_error(L, "Exception when reading path argument %d: %s\n", arg_num, re.what());
                }
                out_path = std::get<std::u16string>(wdirectory);
                break;
            }
            case LUA_TUSERDATA:
            {
                fs::path& path = *static_cast<fs::path*>(lua_touserdata(L, index));
                out_path = path;
                break;
            }
            default:
                return luaL_argerror(L, arg_num, "Expected string or stdfs::path");
        }
    }
    catch (fs::filesystem_error const& fse)
    {
        return luaL_error(L, "Exception when reading path argument %d: %s\n", arg_num, fse.what());
    }
    return 0;
}

int stdfs::path_ctor(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    return push_path(L, path);
}

//////////////////////////////////////////////////////////////////////
static int get_string_from_path(lua_State* L, int idx, std::string& str)
{
    str.clear();
    int type = lua_type(L, idx);

    if (type == LUA_TSTRING)
    {
        str = luaL_checkstring(L, idx);
    }
    else if (type == LUA_TUSERDATA)
    {
        fs::path* path = static_cast<fs::path*>(luaL_checkudata(L, idx, s_str_meta_path));
        if (!path)
        {
            return luaL_argerror(L, idx, "Unknown user data type, expected stdfs.path");
        }
        auto converted_str = stdfs::convert_utf16_utf8(path->u16string());
        if (converted_str.index() > 0)
        {
            auto re = std::get<std::range_error>(converted_str);
            return luaL_error(L, "Exception during conversion of argument %d: %s\n", idx, re.what());
        }
        str = std::get<std::string>(converted_str);
    }
    return 0;
}

static int path_concat(lua_State* L)
{
    std::string lhs, rhs;
    get_string_from_path(L, 1, lhs);
    get_string_from_path(L, 2, rhs);
    lua_pushstring(L, (lhs + rhs).c_str());
    return 1;
}

static int path_div(lua_State* L)
{
    fs::path lhs_path, rhs_path;
    get_path(L, lhs_path, 1, -2);
    get_path(L, rhs_path, 1, -1);
    lhs_path /= rhs_path;
    push_path(L, lhs_path);
    return 1;
}

int stdfs::path_equivalent(lua_State* L)
{
    fs::path lhs_path, rhs_path;
    get_path(L, lhs_path, 1, 1);
    get_path(L, rhs_path, 2, 2);
    std::error_code ec;
    bool equiv = fs::equivalent(lhs_path, rhs_path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    lua::pushbool(L, equiv);
    return 1;
}

static int path_lexically_normal(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    return push_path(L, path.lexically_normal());
}

static int path_lexically_proximate_closure(lua_State* L)
{
    fs::path path, base;
    get_path(L, path, 0, lua_upvalueindex(1));
    get_path(L, base, 1, 1);
    path.lexically_proximate(base);
    push_path(L, path);
    return 1;
}

static int path_lexically_proximate(lua_State* L)
{
    lua_pushcclosure(L, &path_lexically_proximate_closure, 1);
    return 1;
}

static int path_lexically_relative_closure(lua_State* L)
{
    fs::path path, base;
    get_path(L, path, 0, lua_upvalueindex(1));
    get_path(L, base, 1, 1);
    path.lexically_relative(base);
    push_path(L, path);
    return 1;
}

static int path_lexically_relative(lua_State* L)
{
    lua_pushcclosure(L, &path_lexically_relative_closure, 1);
    return 1;
}

static int path_replace_filename_closure(lua_State* L)
{
    fs::path path, replacement;
    get_path(L, path, 1, lua_upvalueindex(1));
    if (lua_gettop(L) > 0)
    {
        get_path(L, replacement, 1, 1);
    }
    path.replace_filename(replacement);
    push_path(L, path);
    return 1;
}

static int path_replace_filename(lua_State* L)
{
    lua_pushcclosure(L, &path_replace_filename_closure, 1);
    return 1;
}

static int path_replace_extension_closure(lua_State* L)
{
    fs::path path, replacement;
    get_path(L, path, 1, lua_upvalueindex(1));
    if (lua_gettop(L) > 0)
        get_path(L, replacement, 1, 1);
    path.replace_extension(replacement);
    push_path(L, path);
    return 1;
}

static int path_replace_extension(lua_State* L)
{
    lua_pushcclosure(L, &path_replace_extension_closure, 1);
    return 1;
}

static int path_gc(lua_State* L)
{
    fs::path* path = static_cast<fs::path*>(lua_touserdata(L, 1));
    path->~path();
    return 1;
}

static int path_tostring(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    auto converted_str = stdfs::convert_utf16_utf8(path.u16string());
    if (converted_str.index() > 0)
    {
        auto re = std::get<std::range_error>(converted_str);
        return luaL_error(L, "Exception during conversion of argument 1: %s\n", re.what());
    }
    std::string const& path_str = std::get<std::string>(converted_str);
    lua_pushstring(L, path_str.c_str());
    return 1;
}

int stdfs::absolute(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    std::error_code ec;
    fs::path const abs_path = fs::absolute(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return push_path(L, abs_path);
}

int stdfs::canonical(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    bool weak_canonical = false;
    if (lua_gettop(L) > 1 && lua_isboolean(L, 2))
    {
        weak_canonical = lua::tobool(L, 2);
    }
    std::error_code ec;
    fs::path const can_path = weak_canonical ? fs::weakly_canonical(path, ec) : fs::canonical(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return push_path(L, can_path);
}

int stdfs::is_absolute(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    try
    {
        bool const is_absolute = path.is_absolute();
        lua::pushbool(L, is_absolute);
        return 1;
    }
    catch (std::exception const& ex)
    {
        return luaL_error(L, "is_absolute() caused exception: %s\n", ex.what());
    }
}

int stdfs::is_relative(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    try
    {
        bool const is_relative = path.is_relative();
        lua::pushbool(L, is_relative);
        return 1;
    }
    catch (std::exception const& ex)
    {
        return luaL_error(L, "is_relative() caused exception: %s\n", ex.what());
    }
}

int stdfs::proximate(lua_State* L)
{
    fs::path path, base;
    get_path(L, path, 1, 1);
    get_path(L, base, 2, 2);
    std::error_code ec;
    fs::path const prox_path = fs::proximate(path, base, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return push_path(L, prox_path);
}

int stdfs::relative(lua_State* L)
{
    fs::path path, base;
    get_path(L, path, 1, 1);
    get_path(L, base, 2, 2);
    std::error_code ec;
    fs::path const rel_path = fs::relative(path, base, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return push_path(L, rel_path);
}

static int path_extension(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    return push_path(L, path.extension());
}

static int path_filename(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    return push_path(L, path.filename());
}

static int path_parent_path(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    return push_path(L, path.parent_path());
}

static int path_stem(lua_State* L)
{
    fs::path path;
    get_path(L, path);
    return push_path(L, path.stem());
}

static int path_index(lua_State* L)
{
    std::string field_name = lua_tostring(L, -1);
    lua_pop(L, 1);

    std::pair<std::string, lua_CFunction> map[] = {
            { "absolute",               &absolute },
            { "canonical",              &canonical },
            { "is_absolute",            &is_absolute },
            { "is_relative",            &is_relative },
            { "lexically_normal",       &path_lexically_normal },
            { "lexically_proximate",    &path_lexically_proximate },
            { "lexically_relative",     &path_lexically_relative },
            { "ext",                    &path_extension },
            { "filename",               &path_filename },
            { "parent",                 &path_parent_path },
            { "stem",                   &path_stem },
            { "repl_filename",          &path_replace_filename },
            { "repl_ext",               &path_replace_extension },
    };
    for (auto const& elem: map)
    {
        if (field_name == elem.first)
            return elem.second(L);
    }
    lua_pushnil(L);
    return 1;
}
//////////////////////////////////////////////////////////////////////

int stdfs::check_exists_path_meta_table(lua_State* L)
{
    // first push metatable, then check it's type
    // for backward compatibility with Lua <= 5.2
    // in Lua >= 5.3, luaL_getmetatable returns that type
    luaL_getmetatable(L, s_str_meta_path);
    if (lua_type(L, lua_gettop(L)) != LUA_TNIL)
        return luaL_error(L, "Metatable %s already defined, can't load module", s_str_meta_path);
    return 0;
}

int stdfs::create_path_meta_table(lua_State* L)
{
    luaL_newmetatable(L, s_str_meta_path);

    lua_pushcfunction(L, path_concat);
    lua_setfield(L, -2, "__concat");
    lua_pushcfunction(L, path_div);
    lua_setfield(L, -2, "__div");
    lua_pushcfunction(L, path_equivalent);
    lua_setfield(L, -2, "__eq");
    lua_pushcfunction(L, path_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushcfunction(L, path_tostring);
    lua_setfield(L, -2, "__tostring");

    lua_pushcfunction(L, path_index);
    lua_setfield(L, -2, "__index");

    return 1;
}
