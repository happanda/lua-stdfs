#include "common.h"
#include "exists.h"
#include "lua_wrappers.h"

namespace fs = std::filesystem;


int stdfs::exists(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool path_exists = fs::exists(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    lua::pushbool(L, path_exists);
    return 1;
}
