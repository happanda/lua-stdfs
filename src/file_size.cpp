#include "common.h"
#include "file_size.h"

namespace fs = std::filesystem;


int stdfs::file_size(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    std::uintmax_t size = fs::file_size(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    lua_pushinteger(L, size);
    return 1;
}
