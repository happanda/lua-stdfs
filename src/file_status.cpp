#include "common.h"
#include "file_status.h"

namespace fs = std::filesystem;

int stdfs::file_status(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    fs::file_status const status = fs::status(path, ec);
    // Intentionally not throwing, because there is file_type::not_found
    // Although we ignore other possible OS API errors here?
//    if (ec) {
//        return luaL_error(L, ec.message().c_str());
//    }
    fs::file_type const tp = status.type();
    fs::perms const pm = status.permissions();

    lua_newtable(L);
    lua_pushinteger(L, static_cast<int>(tp));
    lua_setfield(L, -2, "file_type");
    lua_pushinteger(L, static_cast<int>(pm));
    lua_setfield(L, -2, "file_perms");
    return 1;
}

int stdfs::symlink_status(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    fs::file_status const status = fs::symlink_status(path, ec);
    // Same argument as with file_status error reporting
//    if (ec) {
//        return luaL_error(L, ec.message().c_str());
//    }
    fs::file_type const tp = status.type();
    fs::perms const pm = status.permissions();

    lua_newtable(L);
    lua_pushinteger(L, static_cast<int>(tp));
    lua_setfield(L, -2, "file_type");
    lua_pushinteger(L, static_cast<int>(pm));
    lua_setfield(L, -2, "file_perms");
    return 1;
}

int stdfs::create_file_type_table(lua_State* L)
{
    std::pair<fs::file_type, char const*> map[] =
    {
            { fs::file_type::none,      "none" },
            { fs::file_type::not_found, "not_found" },
            { fs::file_type::regular,   "regular" },
            { fs::file_type::directory, "directory" },
            { fs::file_type::symlink,   "symlink" },
            { fs::file_type::block,     "block" },
            { fs::file_type::character, "character" },
            { fs::file_type::fifo,      "fifo" },
            { fs::file_type::socket,    "socket" },
            { fs::file_type::unknown,   "unknown" },
    };

    lua_newtable(L);
    for (auto const& p : map)
    {
        int const intVal = static_cast<int>(p.first);
        lua_pushinteger(L, intVal);
        lua_setfield(L, -2, p.second);
    }
    return 1;
}
