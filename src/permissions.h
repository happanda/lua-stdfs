#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Changes file permissions if possible.
     *
     * @return \a 0 in case of success
     * @return Result of luaL_error() in case of error
     */
    int permissions(lua_State* L);


    /**
     * @brief Pushes on stack a table with file permission values
     * @return \a 1 (number of objects pushed on stack)
     *
     * Tables fields: <em>none, owner_read, owner_write, owner_exec, owner_all, group_read, group_write, group_exec,
     * group_all, others_read, others_write, others_exec, others_all, all, set_uid, set_gid, sticky_bit, mask,
     * unknown.</em>
     * Values can be ORed.
     */
    int create_file_permissions_table(lua_State* L);

    /**
     * @brief Pushes on stack a table with file permission options values
     * @return \a 1 (number of objects pushed on stack)
     *
     * Tables fields: <em>replace, add, remove, nofollow.</em>
     * Options can be ORed.
     */
    int create_file_permission_options_table(lua_State* L);
} // namespace stdfs
