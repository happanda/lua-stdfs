#include "common.h"
#include "work_dir.h"

namespace fs = std::filesystem;


int stdfs::work_dir(lua_State* L)
{
    if (lua_gettop(L) > 0)
    {
        fs::path path;
        get_path(L, path);

        try
        {
            fs::current_path(path);
            return 0;
        }
        catch (std::range_error const& re)
        {
            return luaL_error(L, re.what());
        }
        catch (fs::filesystem_error const& fse)
        {
            return luaL_error(L, fse.what());
        }
    }
    else
    {
        try
        {
            auto pwd = fs::current_path();
            return push_path(L, pwd);
        }
        catch (std::range_error const& re)
        {
            return luaL_error(L, re.what());
        }
        catch (fs::filesystem_error const& fse)
        {
            return luaL_error(L, fse.what());
        }
    }
}
