#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief User facing API for copying files and directories
     * @return \a 0 in case of successful operation
     * @return Result of \a luaL_error() in case of error
     */
    int copy(lua_State* L);

    /**
     * @brief Creates \a stdfs.copy_opts table with of \p copy() operation options
     * @return \a 1 (number of objects pushed on stack)
     */
    int create_copy_opts_table(lua_State* L);
} // namespace stdfs
