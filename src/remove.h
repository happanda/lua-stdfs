#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Removes target file or directory
     * @return \a 1 as one boolean value is pushed on stack - was target removed or not
     * @return Result of luaL_error() in case of error
     *
     * Details of the operation can be found at https://en.cppreference.com/w/cpp/filesystem/remove
     */
    int remove(lua_State* L);

    /**
     * @brief Removes file or a directory recursively
     * @return \a 1 as one integer value is pushed on stack - number of objects removed
     * @return Result of luaL_error() in case of error
     *
     * Details of the operation can be found at https://en.cppreference.com/w/cpp/filesystem/remove
     */
    int remove_all(lua_State* L);
} // namespace stdfs
