#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Queries last modification time of a filesystem object
     *
     * @return \a 1 (as one table is pushed on stack)
     * @return Result of luaL_error() in case of error
     *
     * A table is pushed on stack containing GMT time of last modification:
     * <em>sec, min, hour, mday, mon, year, wday, yday, isdst, since_epoch, ctm, loc</em>
     * In turn, \e loc is a table containing local time of last modification:
     * <em>sec, min, hour, mday, mon, year, wday, yday, since_epoch</em>
     */
    int write_time(lua_State* L);
} // namespace stdfs
