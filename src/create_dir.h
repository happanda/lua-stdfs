#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Creates a directory recursively (including all parent directories)
     * @return \a 1 in case of success, as one boolean value is pushed on stack - whether directory was created
     * @return Result of \a luaL_error() in case of error
     */
    int create_dir(lua_State* L);
} // namespace stdfs
