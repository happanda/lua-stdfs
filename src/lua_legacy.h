#pragma once
#include <lua.hpp>
#include <array>
#include <cstdint>

#if LUA_VERSION_NUM <= 501
#define luaL_newlibtable(L,l)	\
    lua_createtable(L, 0, sizeof(l)/sizeof((l)[0]) - 1)
#define luaL_newlib(L,l)  \
    (luaL_newlibtable(L,l), luaL_register(L,nullptr,l))
#define LUA_OK 0
#endif

namespace stdfs {
    namespace lua {
        template<size_t N>
        void newlib(lua_State* L, std::array<luaL_Reg, N> const& funcs) {
#if LUA_VERSION_NUM <= 501
            lua_createtable(L, 0, funcs.size() - 1);
            luaL_register(L, nullptr, funcs.data());

#else
            luaL_checkversion(L);
            lua_createtable(L, 0, funcs.size() - 1);
            luaL_setfuncs(L, funcs.data(), 0);
#endif
        }

        int getglobal(lua_State* L, char const* name);
        int getfield(lua_State* L, int idx, char const* name);
        int getmetafield(lua_State* L, int idx, char const* name);
        int gettable(lua_State* L, int idx);
        int geti(lua_State* L, int idx, lua_Integer i);
        bool isinteger(lua_State* L, int idx);
        size_t rawlen(lua_State* L, int idx);
    }
}
