#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Query target path attributes (symlinks are followed).
     *
     * Pushes on stack a table containing \a file_type and \a file_perms fields
     * @return \a 1 (number of objects pushed on stack)
     */
    int file_status(lua_State* L);

    /**
     * @brief Query target symlink path attributes (not following symlinks).
     *
     * Pushes on stack a table containing \a file_type and \a file_perms fields
     * @return \a 1 (number of objects pushed on stack)
     */
    int symlink_status(lua_State* L);

    /**
     * @brief Pushes on stack a table with file type values
     * @return \a 1 (number of objects pushed on stack)
     *
     * Tables fields: <em>none, not_found, regular, directory, symlink, block, character, fifo, socket, unknown.</em>
     */
    int create_file_type_table(lua_State* L);
} // namespace stdfs
