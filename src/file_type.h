#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Checks if given path is a block file
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_block_file(lua_State* L);

    /**
     * @brief Checks if given path is a character file
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_character_file(lua_State* L);

    /**
     * @brief Checks if given path is a directory
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_directory(lua_State* L);

    /**
     * @brief Checks if given path is an empty file or directory
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_empty(lua_State* L);

    /**
     * @brief Checks if given path is a named pipe
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_fifo(lua_State* L);

    /**
     * @brief Checks if given path is not a regular file, directory or symlink
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_other(lua_State* L);

    /**
     * @brief Checks if given path is a regular file
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_regular_file(lua_State* L);

    /**
     * @brief Checks if given path is a socket
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_socket(lua_State* L);

    /**
     * @brief Checks if given path is a symlink
     * @return \a 1 (number of objects pushed on stack)
     */
    int is_symlink(lua_State* L);

}