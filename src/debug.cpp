#include <cstdarg>
#include <cstdio>

#include "debug.h"


int stdfs::debug_print([[maybe_unused]] char const* format, ...)
{
#ifdef DEBUG
    std::va_list args;
    va_start(args, format);
    int res = vprintf(format, args);
    va_end(args);
    return res;
#else
    return 0;
#endif
}
