#include "lua_legacy.h"


int stdfs::lua::getglobal(lua_State* L, char const* name) {
#if LUA_VERSION_NUM <= 502
    lua_getglobal(L, name);
    return lua_type(L, lua_gettop(L));
#else
    return lua_getglobal(L, name);
#endif
}

int stdfs::lua::getfield(lua_State* L, int idx, char const* name) {
#if LUA_VERSION_NUM <= 502
    lua_getfield(L, idx, name);
    return lua_type(L, lua_gettop(L));
#else
    return lua_getfield(L, idx, name);
#endif
}

int stdfs::lua::getmetafield(lua_State* L, int idx, char const* name) {
#if LUA_VERSION_NUM <= 502
    if (luaL_getmetafield(L, idx, name) != 0) {
        return lua_type(L, lua_gettop(L));
    }
    return 0;
#else
    return luaL_getmetafield(L, idx, name);
#endif
}

int stdfs::lua::gettable(lua_State* L, int idx) {
#if LUA_VERSION_NUM <= 502
    ::lua_gettable(L, idx);
    return lua_type(L, lua_gettop(L));
#else
    return ::lua_gettable(L, idx);
#endif
}

int stdfs::lua::geti(lua_State* L, int idx, lua_Integer i) {
#if LUA_VERSION_NUM <= 502
    lua_pushinteger(L, i);
    if (idx < 0) {
        --idx;
    }
    ::lua_gettable(L, idx);
    return lua_type(L, lua_gettop(L));
#else
    return ::lua_geti(L, idx, i);
#endif
}

bool stdfs::lua::isinteger(lua_State* L, int idx) {
#if LUA_VERSION_NUM <= 502
    // from luaL_checkinteger: https://www.lua.org/source/5.1/lauxlib.c.html#luaL_checkinteger
    lua_Integer d = lua_tointeger(L, idx);
    return (d != 0 || lua_isnumber(L, idx));
#else
    return ::lua_isinteger(L, idx);
#endif
}

size_t stdfs::lua::rawlen(lua_State* L, int idx) {
#if LUA_VERSION_NUM <= 501
    return lua_objlen(L, idx);
#else
    return lua_rawlen(L, idx);
#endif
}
