#include "common.h"
#include "read_symlink.h"

namespace fs = std::filesystem;


int stdfs::read_symlink(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    const fs::path target = fs::read_symlink(path, ec);

    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return push_path(L, target);
}
