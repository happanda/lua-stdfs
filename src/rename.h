#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Moves or renames filesystem object
     * @return \a 0 if operation is successful
     * @return Result of luaL_error() in case of error
     *
     * Details of the operation can be found at https://en.cppreference.com/w/cpp/filesystem/rename
     */
    int rename(lua_State* L);
} // namespace stdfs
