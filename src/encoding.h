#pragma once

#include <string>
#include <variant>
namespace std
{
	class range_error;
}


namespace stdfs
{
	std::variant<std::u16string, std::range_error> convert_utf8_utf16(char const* utf8str);

	std::variant<std::string, std::range_error> convert_utf16_utf8(std::u16string const& utf16str);
}
