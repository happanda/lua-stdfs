#include <stdexcept>
#include "encoding.h"

#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
#include <codecvt>
#include <locale>

using convert_type = std::codecvt_utf8_utf16<char16_t>;
static std::wstring_convert<convert_type, typename convert_type::intern_type> s_converter;


std::variant<std::u16string, std::range_error> stdfs::convert_utf8_utf16(char const* utf8str)
{
	std::u16string converted_str;
	try
	{
		converted_str = s_converter.from_bytes(utf8str);
	}
	catch (std::range_error const& ex)
	{
		return ex;
	}
	return converted_str;
}

std::variant<std::string, std::range_error> stdfs::convert_utf16_utf8(std::u16string const& utf16str)
{
	std::string converted_str;
	try
	{
		converted_str = s_converter.to_bytes(utf16str);
	}
	catch (std::range_error const& ex)
	{
		return ex;
	}
	return converted_str;
}
