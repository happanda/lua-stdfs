#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Pushes a C closure on stack that is an iterator over folder contents
     * @return \a 1 in case of successful operation, as one value is pushed on stack
     * @return Result of \a luaL_error() in case of errors
     */
    int ls(lua_State* L);

    /**
     * @brief Checks if a meta table \a stdfs.meta_ls already exists
     * @return \a 0 if table doesn't exist
     * @return Result of \a luaL_error() if table already exists
     */
    int check_exists_ls_meta_table(lua_State* L);

    /**
     * @brief Creates (or overwrites) \a stdfs.meta_ls metatable
     * @return \a 1 (as one table is pushed on stack)
     *
     * This table is used for directory iterators. Includes garbage collection __gc function.
     */
    int create_ls_meta_table(lua_State* L);

    /**
     * @brief Pushes on stack a table with directory traversal options
     * @return \a 1 (as one table is pushed on stack)
     *
     * Table fields: <em> none, follow_directory_symlink, skip_permission_denied </em>
     */
    int create_dir_opts_table(lua_State* L);
} // namespace stdfs
