#pragma once

struct lua_State;

namespace stdfs {
    namespace lua {
        /**
         * @brief Pushes a boolean value onto stack L
         * @return \a 1 - number of values pushed onto stack
         */
        int pushbool(lua_State* L, bool value);

        /**
         * @brief Converts the value at \a index to boolean
         * @return boolean of a value at \a index
         */
        bool tobool(lua_State* L, int index);
    }
}
