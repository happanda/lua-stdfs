#include "common.h"
#include "create_hardlink.h"

namespace fs = std::filesystem;


int stdfs::create_hardlink(lua_State* L)
{
    fs::path target_path, link_path;
    get_path(L, target_path, 1, 1);
    get_path(L, link_path, 2, 2);

    std::error_code ec;
    fs::create_hard_link(target_path, link_path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    return 0;
}
