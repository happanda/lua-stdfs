#include "common.h"
#include "hardlink_count.h"

namespace fs = std::filesystem;


int stdfs::hardlink_count(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    uintmax_t const hl_count = fs::hard_link_count(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }
    // TODO: think about this static_cast type range
    lua_pushinteger(L, static_cast<lua_Integer>(hl_count));
    return 1;
}
