#include <variant>
#include "common.h"
#include "encoding.h"
#include "ls.h"
#include "lua_wrappers.h"

namespace fs = std::filesystem;
using namespace stdfs;
using fs_dir_iter = fs::directory_iterator;
using fs_rec_dir_iter = fs::recursive_directory_iterator;
using fs_var_dir_iter = std::variant<fs_dir_iter, fs_rec_dir_iter>;

char const* s_str_meta_ls = "stdfs.meta_ls";


static int ls_closure(lua_State* L)
{
    auto* wrap = static_cast<fs_var_dir_iter*>(
            luaL_checkudata(L, lua_upvalueindex(1), s_str_meta_ls));
    return std::visit([L](auto&& arg)->int
                      {
                          if (arg == std::decay_t<decltype(arg)>{})
                          {
                              lua_pushnil(L);
                              return 1;
                          }

                          try
                          {
                              push_path(L, arg->path());
                              ++arg;
                          }
                          catch (fs::filesystem_error const& fse)
                          {
                              return luaL_error(L, fse.what());
                          }
                          return 1;
                      }, *wrap);
}

int stdfs::ls(lua_State* L)
{
    bool recursive = false;
    fs::directory_options dir_opts = fs::directory_options::skip_permission_denied;
    if (lua_gettop(L) > 1)
    {
        int const luaTop = lua_gettop(L);
        for (int k = 2; k <= luaTop; ++k)
        {
            if (lua_isboolean(L, k))
            {
                recursive = (lua::tobool(L, k) != 0);
            }
            else if (stdfs::lua::isinteger(L, k) == 1)
            {
                lua_Integer dirOptsInt = lua_tointeger(L, k);
                dir_opts = static_cast<fs::directory_options>(dirOptsInt);
            }
        }
    }

    fs::path path;
    get_path(L, path, 1, 1);

    try
    {
        if (!fs::exists(path))
        {
            return luaL_error(L, "Target directory doesn't exist");
        }
        if (!fs::is_directory(path))
        {
            return luaL_error(L, "Target path is not a directory");
        }

        void* dir_iter_udata = lua_newuserdata(L, sizeof(fs_var_dir_iter));
        if (recursive)
        {
            new(dir_iter_udata) fs_var_dir_iter(fs_rec_dir_iter(
                    path, dir_opts));
        }
        else
        {
            new(dir_iter_udata) fs_var_dir_iter(fs_dir_iter(
                    path, dir_opts));
        }
    }
    catch (fs::filesystem_error const& fse)
    {
        return luaL_error(L, fse.what());
    }

    luaL_getmetatable(L, s_str_meta_ls);
    lua_setmetatable(L, -2);
    lua_pushcclosure(L, &ls_closure, 1);
    return 1;
}

static int ls_gc(lua_State* L)
{
    auto* var_dir_iter = static_cast<fs_var_dir_iter*>(lua_touserdata(L, 1));
    var_dir_iter->~variant();
    return 1;
}

int stdfs::check_exists_ls_meta_table(lua_State* L)
{
    // first push metatable, then check it's type
    // for backward compatibility with Lua <= 5.2
    // in Lua >= 5.3, luaL_getmetatable returns that type
    luaL_getmetatable(L, s_str_meta_ls);
    if (lua_type(L, lua_gettop(L)) != LUA_TNIL)
        return luaL_error(L, "Metatable %s already defined, can't load module", s_str_meta_ls);
    return 0;
}

int stdfs::create_ls_meta_table(lua_State* L)
{
    luaL_newmetatable(L, s_str_meta_ls);
    lua_pushcfunction(L, ls_gc);
    lua_setfield(L, -2, "__gc");
    return 1;
}

int stdfs::create_dir_opts_table(lua_State* L)
{
    std::pair<fs::directory_options, char const*> map[] =
            {
                    { fs::directory_options::none,                     "none" },
                    { fs::directory_options::follow_directory_symlink, "follow_directory_symlink" },
                    { fs::directory_options::skip_permission_denied,   "skip_permission_denied" },
            };

    lua_newtable(L);
    for (auto const& p: map)
    {
        int const intVal = static_cast<int>(p.first);
        lua_pushinteger(L, intVal);
        lua_setfield(L, -2, p.second);
    }
    return 1;
}
