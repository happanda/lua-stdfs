#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Creates a hard link
     * @return \a 0 as nothing is pushed on stack
     * @return Result of \a luaL_error() in case of error
     */
    int create_hardlink(lua_State* L);
} // namespace stdfs
