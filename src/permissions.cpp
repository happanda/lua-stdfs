#include "common.h"
#include "permissions.h"

namespace fs = std::filesystem;

int stdfs::permissions(lua_State* L)
{
    if (lua_gettop(L) < 2)
    {
        return luaL_error(L, "permissions: requires more arguments: <path> <permissions> [<permission_options>]");
    }
    if (stdfs::lua::isinteger(L, 2) != 1)
    {
        return luaL_error(L, "permissions: <permissions> argument must be taken from stdfs.file_perms table");
    }

    fs::path path;
    get_path(L, path, 1, 1);

    fs::perms const prms = static_cast<fs::perms>(lua_tointeger(L, 2));
    fs::perm_options perm_opts = fs::perm_options::replace;

    if (lua_gettop(L) > 2)
    {
        if (stdfs::lua::isinteger(L, 3) != 1)
        {
            return luaL_error(L,
                              "permissions: <permission_options> argument must be taken from stdfs.perm_options table");
        }
        else
        {
            perm_opts = static_cast<fs::perm_options>(lua_tointeger(L, 3));
        }
    }

    std::error_code ec;
    fs::permissions(path, prms, perm_opts, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }

    return 0;
}


int stdfs::create_file_permissions_table(lua_State* L)
{
    std::pair<fs::perms, char const*> map[] =
        {
            { fs::perms::none,			"none" },
            { fs::perms::owner_read,	"owner_read" },
            { fs::perms::owner_write,	"owner_write" },
            { fs::perms::owner_exec,	"owner_exec" },
            { fs::perms::owner_all,		"owner_all" },
            { fs::perms::group_read,	"group_read" },
            { fs::perms::group_write,	"group_write" },
            { fs::perms::group_exec,	"group_exec" },
            { fs::perms::group_all,		"group_all" },
            { fs::perms::others_read,	"others_read" },
            { fs::perms::others_write,	"others_write" },
            { fs::perms::others_exec,	"others_exec" },
            { fs::perms::others_all,	"others_all" },
            { fs::perms::all,			"all" },
            { fs::perms::set_uid,		"set_uid" },
            { fs::perms::set_gid,		"set_gid" },
            { fs::perms::sticky_bit,	"sticky_bit" },
            { fs::perms::mask,			"mask" },
            { fs::perms::unknown,		"unknown" },
        };

    lua_newtable(L);
    for (auto const& p : map) {
        int const intVal = static_cast<int>(p.first);
        lua_pushinteger(L, intVal);
        lua_setfield(L, -2, p.second);
    }
    return 1;
}

int stdfs::create_file_permission_options_table(lua_State* L)
{
    std::pair<fs::perm_options, char const*> map[] =
            {
                    { fs::perm_options::replace,    "replace" },
                    { fs::perm_options::add,        "add" },
                    { fs::perm_options::remove,     "remove" },
                    { fs::perm_options::nofollow,   "nofollow" },
            };

    lua_newtable(L);
    for (auto const& p : map) {
        int const intVal = static_cast<int>(p.first);
        lua_pushinteger(L, intVal);
        lua_setfield(L, -2, p.second);
    }
    return 1;
}
