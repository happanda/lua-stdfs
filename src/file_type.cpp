#include "common.h"
#include "file_type.h"
#include "lua_wrappers.h"

namespace fs = std::filesystem;

int stdfs::is_block_file(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_block = fs::is_block_file(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_block);
}

int stdfs::is_character_file(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_character = fs::is_character_file(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_character);
}

int stdfs::is_directory(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_directory = fs::is_directory(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_directory);
}

int stdfs::is_empty(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_empty = fs::is_empty(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_empty);
}

int stdfs::is_fifo(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_fifo = fs::is_fifo(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_fifo);
}

int stdfs::is_other(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_other = fs::is_other(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_other);
}

int stdfs::is_regular_file(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_regular = fs::is_regular_file(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_regular);
}

int stdfs::is_socket(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_socket = fs::is_socket(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_socket);
}

int stdfs::is_symlink(lua_State* L) {
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    bool const is_symlink = fs::is_symlink(path, ec);
    if (ec) {
        return luaL_error(L, ec.message().c_str());
    }
    return lua::pushbool(L, is_symlink);
}

