#include "stdfs.h"
#include "lua_legacy.h"
#include "version.h"

#include <lua.hpp>

#include <array>
#include <string>

#ifdef _MSC_VER
#define DLL_EXPORT __declspec(dllexport)
#elif __GNUC__
#define DLL_EXPORT __attribute__((visibility("default")))
#endif

struct Global
{
    const char* name{ nullptr };
    const char* value{ nullptr };
};

const std::array<Global, 5> stdfs_globals =
{
    Global{ "_NAME", "stdfs" },
    Global{ "_DESCRIPTION", "Filesystem API library" },
    Global{ "_VERSION", stdfs::VERSION_STRING },
    Global{ "_LICENSE", "MIT" },
    Global{ "_URL", "https://gitlab.com/happanda/lua-stdfs" }
};

void stdfs_create_globals(lua_State* L)
{
    for (auto&& var: stdfs_globals)
    {
        lua_pushstring(L, var.value);
        lua_setfield(L, -2, var.name);
    }
}

std::array<luaL_Reg, 38> stdfs_funcs =
{
    luaL_Reg
    { "absolute",        &stdfs::absolute },
    { "canonical",       &stdfs::canonical },
    { "copy",            &stdfs::copy },
    { "create_dir",      &stdfs::create_dir },
    { "create_hardlink", &stdfs::create_hardlink },
    { "create_symlink",  &stdfs::create_symlink },
    { "exists",          &stdfs::exists },
    { "equiv",           &stdfs::path_equivalent },
    { "file_size",       &stdfs::file_size },
    { "file_status",     &stdfs::file_status },
    { "hardlink_count",  &stdfs::hardlink_count },
    { "is_absolute",     &stdfs::is_absolute },
    { "is_relative",     &stdfs::is_relative },

    { "is_block_file",      &stdfs::is_block_file },
    { "is_character_file",  &stdfs::is_character_file },
    { "is_directory",       &stdfs::is_directory },
    { "is_empty",           &stdfs::is_empty },
    { "is_fifo",            &stdfs::is_fifo },
    { "is_other",           &stdfs::is_other },
    { "is_regular_file",    &stdfs::is_regular_file },
    { "is_socket",          &stdfs::is_socket },
    { "is_symlink",         &stdfs::is_symlink },

    { "ls",              &stdfs::ls },
    { "path",            &stdfs::path_ctor },
    { "permissions",     &stdfs::permissions },
    { "proximate",       &stdfs::proximate },
    { "read_symlink",    &stdfs::read_symlink },
    { "relative",        &stdfs::relative },
    { "remove",          &stdfs::remove },
    { "remove_all",      &stdfs::remove_all },
    { "rename",          &stdfs::rename },
    { "resize",          &stdfs::resize_file },
    { "space",           &stdfs::space },
    { "symlink_status",  &stdfs::symlink_status },
    { "temp_dir",        &stdfs::temp_dir },
    { "work_dir",        &stdfs::work_dir },
    { "write_time",      &stdfs::write_time },
    { nullptr,           nullptr },
};

extern "C" {
[[maybe_unused]] DLL_EXPORT int luaopen_stdfs(lua_State* L)
{
    if (stdfs::check_exists_ls_meta_table(L) == 0
        && stdfs::check_exists_path_meta_table(L) == 0)
    {
        stdfs::create_ls_meta_table(L);
        stdfs::create_path_meta_table(L);

        stdfs::lua::newlib(L, stdfs_funcs);

        stdfs::create_copy_opts_table(L);
        lua_setfield(L, -2, "copy_opts");
        stdfs::create_dir_opts_table(L);
        lua_setfield(L, -2, "dir_opts");
        stdfs::create_file_type_table(L);
        lua_setfield(L, -2, "file_type");
        stdfs::create_file_permissions_table(L);
        lua_setfield(L, -2, "file_perms");
        stdfs::create_file_permission_options_table(L);
        lua_setfield(L, -2, "perm_options");

        stdfs_create_globals(L);

        return 1;
    }
    return 0;
}
}
