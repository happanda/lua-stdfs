#include "common.h"
#include "space.h"

namespace fs = std::filesystem;


int stdfs::space(lua_State* L)
{
    fs::path path;
    get_path(L, path);

    std::error_code ec;
    fs::space_info spinfo = fs::space(path, ec);
    if (ec)
    {
        return luaL_error(L, ec.message().c_str());
    }

    lua_newtable(L);
    lua_pushinteger(L, spinfo.capacity);
    lua_setfield(L, -2, "capacity");
    lua_pushinteger(L, spinfo.free);
    lua_setfield(L, -2, "free");
    lua_pushinteger(L, spinfo.available);
    lua_setfield(L, -2, "available");

    return 1;
}
