#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Constructor for \a path userdata object
     * @return Result of \a push_path()
     */
    int path_ctor(lua_State* L);

    /**
     * @brief User facing function \a equiv for paths comparison
     * @return \a 1 if comparison successful
     * @return Result of \a luaL_error() in case of error
     */
    int path_equivalent(lua_State* L);

    /**
     * @brief Makes an absolute path from given path
     * @return Result of \p push_path() if successful
     * @return Result of \a luaL_error() in case of error
     *
     * Result is made up as: work_dir() / p
     */
    int absolute(lua_State* L);

    /**
     * @brief Converts path to canonical absolute form (no dot, dot dot or symlinks)
     * @return Result of \p push_path() if successful
     * @return Result of \a luaL_error() in case of error
     *
     * Only works for existing paths. Provide boolean parameter \a true
     * to get a weakly_canonical path for a non-existent path.
     */
    int canonical(lua_State* L);

    /**
     * @brief Checks if given path is absolute, pushes result on stack
     * @return \p 1 as one boolean value is pushed on stack
     * @return Result of \a luaL_error() in case of error
     */
    int is_absolute(lua_State* L);

    /**
     * @brief Checks if given path is relative, pushes result on stack
     * @return \p 1 as one boolean value is pushed on stack
     * @return Result of \a luaL_error() in case of error
     */
    int is_relative(lua_State* L);

    /**
     * @brief Pushes on stack path proximate to other path.
     * @return Result of \p push_path() if successful
     * @return Result of \a luaL_error() in case of error
     */
    int proximate(lua_State* L);

    /**
     * @brief Pushes on stack path relative to other path.
     * @return Result of \p push_path() if successful
     * @return Result of \a luaL_error() in case of error
     */
    int relative(lua_State* L);

    /**
     * @brief Checks if a meta table \a stdfs.meta_path already exists
     * @return \a 0 if table doesn't exist
     * @return Result of \a luaL_error() if table already exists
     */
    int check_exists_path_meta_table(lua_State* L);

    /**
     * @brief Creates (or overwrites) \a path user type metatable called \a stdfs.meta_path
     * @return \a 1 as one table is pushed on stack
     */
    int create_path_meta_table(lua_State* L);
} // namespace stdfs
