#pragma once

// looks like it's not easily possible to forward declare path
// because of additional namespaces compilers put like ::experimental::, ::__cxx11::
// so std::filesystem::path becomes a typedef to some weird beast
using path = std::filesystem::path;

struct lua_State;

namespace stdfs
{
    /**
     * @brief Pushes \p from_path object onto Lua stack
     * @param L Lua interpreter state
     * @param from_path source \a std::filesystem::path object
     * @return \a 1 which is the number of values pushed on stack
     */
    int push_path(lua_State* L, path const& from_path);

    /**
     * @brief Converts a string or path object on the Lua stack to std::filesystem::path
     * @param L Lua interpreter state
     * @param out_path Result \a std::filesystem::path object
     * @param arg_num Argument index used to report error from the API
     * @param index Stack index of the target object
     * @return \a 0 if conversion successful
     * @return result of \a luaL_error() in case of error
     */
    int get_path(lua_State* L, path& out_path, int arg_num = 1, int index = -1);
} // namespace stdfs
