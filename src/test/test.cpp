#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
#include "utils.h"
#include "../lua_legacy.h"
#include "../lua_wrappers.h"
#include <lua.hpp>
#include <algorithm>
#include <codecvt>
#include <locale>
#include <filesystem>
#include <set>
#include <string>
#include <vector>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"

namespace fs = std::filesystem;

static fs::path s_dump_dir("testcase");

int check_lua_file(lua_State* L, char const* file)
{
    const std::string msg{ file };
    int error = luaL_dofile(L, "test_configure.lua");
    if (error != LUA_OK) {
        std::cout << "Failed running test_configure.lua" << std::endl;
    }
    error = luaL_dofile(L, file);
    if (error != LUA_OK) {
        std::cout << "Failed running " << msg << std::endl;
    }
	CHECK_MESSAGE(LUA_OK == error, msg.c_str());
	return error;
}

int check_lua_code(lua_State* L, char const* codeline)
{
	int error = luaL_dostring(L, codeline);
	CHECK_MESSAGE(LUA_OK == error, lua_tostring(L, -1));
	return error;
}

std::vector<char> get_file_contents(char const* path)
{
    std::vector<char> contents;

    std::vector<char> buffer(1024, '\0');
    std::ifstream ifstr(path, std::ios::binary);

    std::streamsize readBytes = 1;
    while (readBytes > 0)
    {
        readBytes = ifstr.readsome(buffer.data(), 1024);
        std::copy_n(buffer.begin(), readBytes, std::back_inserter(contents));
    }
    return contents;
}



TEST_SUITE_BEGIN("Library stdfs test suite");

TEST_CASE("Library loading" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);

	SUBCASE("Module table")
	{
        check_lua_code(L, R"(package.cpath = "./lib?.so;" .. package.cpath;fs = require("stdfs"))");
		CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "fs"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "copy"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "create_dir"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "exists"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "equiv"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "file_size"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "ls"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "path"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "remove"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "rename"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "temp_dir"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "work_dir"));

		CHECK_EQ(LUA_TTABLE, stdfs::lua::getfield(L, 1, "copy_opts"));

        CHECK_EQ(LUA_TSTRING, stdfs::lua::getfield(L, 1, "_NAME"));
        CHECK_EQ(LUA_TSTRING, stdfs::lua::getfield(L, 1, "_DESCRIPTION"));
        CHECK_EQ(LUA_TSTRING, stdfs::lua::getfield(L, 1, "_VERSION"));
        CHECK_EQ(LUA_TSTRING, stdfs::lua::getfield(L, 1, "_LICENSE"));
        CHECK_EQ(LUA_TSTRING, stdfs::lua::getfield(L, 1, "_URL"));
	}

	SUBCASE("Path table")
	{
        check_lua_code(L, R"(package.cpath = "./lib?.so;" .. package.cpath;fs = require("stdfs"))");
		CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "fs"));
		check_lua_code(L, R"(p = fs.path("."))");
		lua_settop(L, 0);
		CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "p"));
		CHECK_NE(nullptr, luaL_checkudata(L, 1, "stdfs.meta_path"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getmetafield(L, 1, "__concat"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getmetafield(L, 1, "__div"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getmetafield(L, 1, "__eq"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getmetafield(L, 1, "__gc"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getmetafield(L, 1, "__tostring"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getmetafield(L, 1, "__index"));
		CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getfield(L, 1, "ext"));
		CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getfield(L, 1, "filename"));
		CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getfield(L, 1, "parent"));
		CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getfield(L, 1, "stem"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "repl_filename"));
		CHECK_EQ(LUA_TFUNCTION, stdfs::lua::getfield(L, 1, "repl_ext"));
	}

	SUBCASE("Copy options table")
	{
        check_lua_code(L, R"(package.cpath = "./lib?.so;" .. package.cpath;fs = require("stdfs"))");
        check_lua_code(L, "cpo = fs.copy_opts");
		lua_settop(L, 0);
		CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "cpo"));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "none"));
		CHECK_EQ((int)fs::copy_options::none, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "skip_existing"));
		CHECK_EQ((int)fs::copy_options::skip_existing, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "overwrite_existing"));
		CHECK_EQ((int)fs::copy_options::overwrite_existing, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "update_existing"));
		CHECK_EQ((int)fs::copy_options::update_existing, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "recursive"));
		CHECK_EQ((int)fs::copy_options::recursive, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "copy_symlinks"));
		CHECK_EQ((int)fs::copy_options::copy_symlinks, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "skip_symlinks"));
		CHECK_EQ((int)fs::copy_options::skip_symlinks, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "directories_only"));
		CHECK_EQ((int)fs::copy_options::directories_only, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "create_symlinks"));
		CHECK_EQ((int)fs::copy_options::create_symlinks, lua_tointeger(L, -1));
		CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "create_hard_links"));
		CHECK_EQ((int)fs::copy_options::create_hard_links, lua_tointeger(L, -1));
	}

	SUBCASE("Meta table already exists 1")
	{
        int error = luaL_dostring(L, R"(package.cpath = "./lib?.so;" .. package.cpath)");
        CHECK_MESSAGE(LUA_OK == error, "package.cpath change have failed");
		luaL_newmetatable(L, "stdfs.meta_ls");
		error = luaL_dostring(L, "fs = require(stdfs)");
		CHECK_MESSAGE(LUA_OK != error, "require(stdfs) must have failed");
        luaL_getmetatable(L, "stdfs.meta_path");
		CHECK_EQ(LUA_TNIL, lua_type(L, lua_gettop(L)));
		lua_settop(L, 0);
        luaL_getmetatable(L, "stdfs.meta_ls");
		CHECK_EQ(LUA_TTABLE, lua_type(L, lua_gettop(L)));

		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "copy"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "create_dir"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "exists"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "equiv"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "ls"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "path"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "remove"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "rename"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "temp_dir"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "work_dir"));
	}

	SUBCASE("Meta table already exists 2")
	{
        int error = luaL_dostring(L, R"(package.cpath = "./lib?.so;" .. package.cpath)");
        CHECK_MESSAGE(LUA_OK == error, "package.cpath change have failed");
		luaL_newmetatable(L, "stdfs.meta_path");
        error = luaL_dostring(L, R"(package.cpath = "./lib?.so;" .. package.cpath)");
        CHECK_MESSAGE(LUA_OK == error, "package.cpath change have failed");
		error = luaL_dostring(L, "fs = require(stdfs)");
		CHECK_MESSAGE(LUA_OK != error, "require(stdfs) must have failed");
        luaL_getmetatable(L, "stdfs.meta_ls");
		CHECK_EQ(LUA_TNIL, lua_type(L, lua_gettop(L)));
        luaL_getmetatable(L, "stdfs.meta_path");
		CHECK_EQ(LUA_TTABLE, lua_type(L, lua_gettop(L)));

		CHECK_EQ(LUA_TNIL, stdfs::lua::getmetafield(L, 1, "__concat"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getmetafield(L, 1, "__div"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getmetafield(L, 1, "__eq"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getmetafield(L, 1, "__gc"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getmetafield(L, 1, "__tostring"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getmetafield(L, 1, "__index"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "ext"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "filename"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "parent"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "stem"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "repl_filename"));
		CHECK_EQ(LUA_TNIL, stdfs::lua::getfield(L, 1, "repl_ext"));
	}

	lua_close(L);
};

TEST_CASE("API: copy" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	check_lua_file(L, "test_copy.lua");

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "cperr1"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "cperr2"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "cperr3"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "cperr4"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "cperr5"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK(fs::exists("testcase/empty-folder/file-1"));
	CHECK(fs::exists("testcase/copy-file-2"));
	// TODO: maybe add contents comparison
	CHECK_EQ(fs::file_size("testcase/non-empty-folder/file-2"), fs::file_size("testcase/copy-file-2"));

	CHECK(fs::exists("testcase/copy-non-empty-folder"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/folder-1"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/folder-2"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/folder-2/file-1"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/folder-2/file-2"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/folder-2/file-3"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/file-1"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/file-2"));
	CHECK(fs::exists("testcase/copy-non-empty-folder/file-3"));
	CHECK_EQ(fs::file_size("testcase/non-empty-folder/file-1"), fs::file_size("testcase/copy-non-empty-folder/file-1"));
	CHECK_EQ(fs::file_size("testcase/non-empty-folder/file-2"), fs::file_size("testcase/copy-non-empty-folder/file-2"));
	CHECK_EQ(fs::file_size("testcase/non-empty-folder/file-3"), fs::file_size("testcase/copy-non-empty-folder/file-3"));
}

TEST_CASE("API: create_dir" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	CHECK(!fs::exists("testcase/create_dir-1"));
	CHECK(!fs::exists("testcase/create_dir-2"));
	CHECK(!fs::exists("testcase/create_dir-2/create_dir-2-1"));

	check_lua_file(L, "test_create_dir.lua");

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "new_folder"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK(fs::exists("testcase/create_dir-1"));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "new_folder_subfolders"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK(fs::exists("testcase/create_dir-2"));
	CHECK(fs::exists("testcase/create_dir-2/create_dir-2-1"));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "existing_folder"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "existing_file"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: equiv" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	check_lua_file(L, "test_equiv.lua");

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "self_equiv_1"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "self_equiv_2"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "self_equiv_3"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "self_unequiv_1"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "self_unequiv_2"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "self_unequiv_3"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "invalid_unequiv_1"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: file_size" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	check_lua_file(L, "test_file_size.lua");

	int const size_1 = s_file_sizes["testcase/non-empty-folder/file-1"];
	int const size_2 = s_file_sizes["testcase/non-empty-folder/file-2"];
	int const size_3 = s_file_sizes["testcase/non-empty-folder/file-3"];

	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "size_1"));
	CHECK_EQ(size_1, lua_tointeger(L, -1));

	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "size_2"));
	CHECK_EQ(size_2, lua_tointeger(L, -1));

	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "size_3"));
	CHECK_EQ(size_3, lua_tointeger(L, -1));

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "size_invalid"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: file_status" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);

    SUBCASE("File status table")
    {
        check_lua_code(L, R"(package.cpath = "./lib?.so;" .. package.cpath;fs = require("stdfs"))");
        check_lua_code(L, "fltp = fs.file_type");
        lua_settop(L, 0);
        CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "fltp"));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "none"));
        CHECK_EQ((int)fs::file_type::none, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "not_found"));
        CHECK_EQ((int)fs::file_type::not_found, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "regular"));
        CHECK_EQ((int)fs::file_type::regular, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "directory"));
        CHECK_EQ((int)fs::file_type::directory, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "symlink"));
        CHECK_EQ((int)fs::file_type::symlink, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "block"));
        CHECK_EQ((int)fs::file_type::block, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "character"));
        CHECK_EQ((int)fs::file_type::character, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "fifo"));
        CHECK_EQ((int)fs::file_type::fifo, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "socket"));
        CHECK_EQ((int)fs::file_type::socket, lua_tointeger(L, -1));
        CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "unknown"));
        CHECK_EQ((int)fs::file_type::unknown, lua_tointeger(L, -1));
    }

    create_testcase_structure(".");

    check_lua_file(L, "test_file_status.lua");

    auto const status_1 = fs::status("testcase/non-empty-folder");
    auto const status_2 = fs::status("testcase/non-empty-folder/file-1");
    auto const status_3 = fs::status("testcase/file-doesnt-exist");

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "status_1"));
    stdfs::lua::getfield(L, -1, "file_type");
    CHECK_EQ(static_cast<int>(status_1.type()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "status_1"));
    stdfs::lua::getfield(L, -1, "file_perms");
    CHECK_EQ(static_cast<int>(status_1.permissions()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "status_2"));
    stdfs::lua::getfield(L, -1, "file_type");
    CHECK_EQ(static_cast<int>(status_2.type()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "status_2"));
    stdfs::lua::getfield(L, -1, "file_perms");
    CHECK_EQ(static_cast<int>(status_2.permissions()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "status_3"));
    stdfs::lua::getfield(L, -1, "file_type");
    CHECK_EQ(static_cast<int>(status_3.type()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "status_3"));
    stdfs::lua::getfield(L, -1, "file_perms");
    CHECK_EQ(static_cast<int>(status_3.permissions()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "status_invalid"));
    CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: file_type" * doctest::no_breaks(true))
{
    // not a comprehensive test, but at least it checks a call to every function
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);

    create_testcase_structure(".");

    check_lua_file(L, "test_file_type.lua");

    std::array<bool const, 9> folder_is = {
        fs::is_block_file("testcase/empty-folder"),
        fs::is_character_file("testcase/empty-folder"),
        fs::is_directory("testcase/empty-folder"),
        fs::is_empty("testcase/empty-folder"),
        fs::is_fifo("testcase/empty-folder"),
        fs::is_other("testcase/empty-folder"),
        fs::is_regular_file("testcase/empty-folder"),
        fs::is_socket("testcase/empty-folder"),
        fs::is_symlink("testcase/empty-folder"),
    };

    std::array<bool const, 9> file_is = {
        fs::is_block_file("testcase/non-empty-folder/file-1"),
        fs::is_character_file("testcase/non-empty-folder/file-1"),
        fs::is_directory("testcase/non-empty-folder/file-1"),
        fs::is_empty("testcase/non-empty-folder/file-1"),
        fs::is_fifo("testcase/non-empty-folder/file-1"),
        fs::is_other("testcase/non-empty-folder/file-1"),
        fs::is_regular_file("testcase/non-empty-folder/file-1"),
        fs::is_socket("testcase/non-empty-folder/file-1"),
        fs::is_symlink("testcase/non-empty-folder/file-1"),
    };

    for (size_t i = 0; i < folder_is.size(); ++i) {
        CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "folder_is"));
        CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::geti(L, -1, static_cast<int>(i + 1)));
        CHECK_EQ(folder_is[i], stdfs::lua::tobool(L, -1));
        lua_pop(L, 1);
    }

    for (size_t i = 0; i < file_is.size(); ++i) {
        CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "file_is"));
        CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::geti(L, -1, static_cast<int>(i + 1)));
        CHECK_EQ(file_is[i], stdfs::lua::tobool(L, -1));
        lua_pop(L, 1);
    }

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "file_type_invalid"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: hardlinks" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

    check_lua_file(L, "test_hardlinks.lua");


    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count01"));
    CHECK_EQ(1, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count11"));
    CHECK_EQ(2, lua_tointeger(L, -1));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count12"));
    CHECK_EQ(2, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count21"));
    CHECK_EQ(3, lua_tointeger(L, -1));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count22"));
    CHECK_EQ(3, lua_tointeger(L, -1));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count23"));
    CHECK_EQ(3, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count32"));
    CHECK_EQ(2, lua_tointeger(L, -1));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count33"));
    CHECK_EQ(2, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "link_count43"));
    CHECK_EQ(1, lua_tointeger(L, -1));
}

TEST_CASE("API ls" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");
	check_lua_file(L, "test_ls.lua");

	SUBCASE("ls")
	{
		std::set<fs::path> foundpaths;
		for (auto const& p : fs::directory_iterator("testcase"))
		{
			foundpaths.insert(p.path());
		}

		CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "lsResult"));
		auto tLen = stdfs::lua::rawlen(L, 1);

		for (decltype(tLen) i = 1; i <= tLen; ++i)
		{
			lua_pushinteger(L, i);
			CHECK_EQ(LUA_TSTRING, stdfs::lua::gettable(L, 1));
			size_t lStr = 0;
			char const* pStr = luaL_checklstring(L, -1, &lStr);
			fs::path p(convert_to_utf8(pStr, pStr + lStr));
			bool found = (foundpaths.find(p) != foundpaths.end());
			CHECK(found);
			foundpaths.erase(p);
			lua_pop(L, 1);
		}
		CHECK(foundpaths.empty());
	}

	SUBCASE("ls recursive")
	{
		std::set<fs::path> foundpaths;
		for (auto const& p : fs::recursive_directory_iterator("testcase"))
		{
			foundpaths.insert(p.path());
		}

		CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "lsRecurResult"));
		auto tLen = stdfs::lua::rawlen(L, 1);

		for (decltype(tLen) i = 1; i <= tLen; ++i)
		{
			lua_pushinteger(L, i);
			CHECK_EQ(LUA_TSTRING, stdfs::lua::gettable(L, 1));
			size_t lStr = 0;
			char const* pStr = luaL_checklstring(L, -1, &lStr);
			fs::path p(convert_to_utf8(pStr, pStr + lStr));
			bool found = (foundpaths.find(p) != foundpaths.end());
			CHECK(found);
			foundpaths.erase(p);
			lua_pop(L, 1);
		}
		CHECK(foundpaths.empty());
	}

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "lsFailResult"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: path" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	check_lua_file(L, "test_path.lua");

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "p0"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "p1"));
	CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "p2"));
	CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "p3"));
	CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "p4"));
    lua_pop(L, 5);

	std::string str;
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "s1"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "s2"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("."), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "s3"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(".."), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "s4"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("."), str);
    lua_pop(L, 4);

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "err1"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "err2"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "err3"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
    lua_pop(L, 3);

	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "op1"));
	CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "op2"));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "op3"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "op4"));
	CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "op5"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "sop1"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("testcase-hello"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "sop2"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(fs::path("testcase") / fs::path("empty-folder"), fs::path(str));
    lua_pop(L, 7);

	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "ext1"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "ext2"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(".bin"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "ext3"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(".dir"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "ext4"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "ext5"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(".ext"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "ext6"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
    lua_pop(L, 6);

	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "filename1"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("empty-folder"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "filename2"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("file-3.bin"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "filename3"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("folder-2.dir"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "filename4"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("."), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "filename5"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("doesnt-exist.filename"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "filename6"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
    lua_pop(L, 6);

	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "parent1"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("testcase"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "parent2"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("testcase"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "parent3"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("testcase/folder-with-folders"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "parent4"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "parent5"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "parent6"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
    lua_pop(L, 6);

	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "stem1"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("empty-folder"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "stem2"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("file-3"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "stem3"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("folder-2"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "stem4"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("."), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "stem5"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string("doesnt-exist"), str);
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "stem6"));
	str = lua_tostring(L, lua_gettop(L));
	CHECK_EQ(std::string(""), str);
    lua_pop(L, 6);
}

TEST_CASE("API: permissions" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

#if LUA_VERSION_NUM <= 502
    check_lua_file(L, "test_permissions_legacy.lua");
#else
    check_lua_file(L, "test_permissions.lua");
#endif

    fs::path const pf1 = fs::path("testcase/non-empty-folder/file-1");
    fs::path const pf2 = fs::path("testcase/non-empty-folder/file-2");
    fs::path const pf3 = fs::path("testcase/empty-folder");
    fs::file_status const fs1 = fs::status(pf1);
    fs::file_status const fs2 = fs::status(pf2);
    fs::file_status const fs3 = fs::status(pf3);
    CHECK_EQ(fs::perms::all, fs1.permissions());
    CHECK_EQ((fs::perms::owner_read | fs::perms::owner_write), fs2.permissions());
    CHECK_EQ(fs::perms::none, (fs3.permissions() & (fs::perms::group_exec | fs::perms::others_exec)));
}

TEST_CASE("API: remove" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

    check_lua_file(L, "test_remove.lua");

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "removed_1"));
    CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "removed_2"));
    CHECK_EQ(true, stdfs::lua::tobool(L, lua_gettop(L)));

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "removed_3"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "removed_4"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

    CHECK(!fs::exists("testcase/empty-folder"));
    CHECK(!fs::exists("testcase/non-empty-folder/file-1"));
    CHECK(fs::exists("testcase/non-empty-folder"));
}

TEST_CASE("API: remove_all" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

    check_lua_file(L, "test_remove_all.lua");

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "removed_1"));
    CHECK_EQ(1, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "removed_2"));
    CHECK_EQ(1, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "removed_3"));
    CHECK_EQ(8, lua_tointeger(L, -1));

    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getglobal(L, "removed_4"));
    CHECK_EQ(0, lua_tointeger(L, -1));

    CHECK(!fs::exists("testcase/empty-folder"));
    CHECK(!fs::exists("testcase/non-empty-folder"));
}

TEST_CASE("API: rename" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	check_lua_file(L, "test_rename.lua");

	CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "rename_invalid"));
	CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

	CHECK(!fs::exists("testcase/non-empty-folder/file-3"));
	CHECK(!fs::exists("testcase/non-empty-folder/file-1"));
	CHECK(fs::exists("testcase/non-empty-folder/file-2"));
	CHECK_EQ(0, fs::file_size("testcase/non-empty-folder/file-2"));

	CHECK(fs::exists("testcase/non-empty-folder"));
	CHECK(!fs::exists("testcase/empty-folder"));
	CHECK(fs::exists("testcase/renamed-empty-folder"));

	CHECK(!fs::exists("testcase/folder-with-folders"));
	CHECK(fs::exists("testcase/renamed-folder-with-folders"));

	CHECK(!fs::exists("testcase/doesnt-exist"));
	CHECK(!fs::exists("testcase/renamed-doesnt-exist"));
}

TEST_CASE("API: resize" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

    auto const fs1 = fs::file_size("testcase/non-empty-folder/file-1");
    auto const fs2 = fs::file_size("testcase/non-empty-folder/file-2");
    auto const fs3 = fs::file_size("testcase/non-empty-folder/file-3");
    auto const cnt1 = get_file_contents("testcase/non-empty-folder/file-1");
    auto const cnt2 = get_file_contents("testcase/non-empty-folder/file-2");
    auto const cnt3 = get_file_contents("testcase/non-empty-folder/file-3");

    check_lua_file(L, "test_resize.lua");

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "rserr1"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "rserr2"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "rserr3"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "rserr4"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));

    auto const fsa1 = fs::file_size("testcase/non-empty-folder/file-1");
    auto const fsa2 = fs::file_size("testcase/non-empty-folder/file-2");
    auto const fsa3 = fs::file_size("testcase/non-empty-folder/file-3");
    CHECK_EQ(fs1 + 20, fsa1);
    CHECK_EQ(fs2 + 30, fsa2);
    CHECK_EQ(fs3 / 2, fsa3);

    auto const cnta1 = get_file_contents("testcase/non-empty-folder/file-1");
    auto const cnta2 = get_file_contents("testcase/non-empty-folder/file-2");
    auto const cnta3 = get_file_contents("testcase/non-empty-folder/file-3");
    CHECK_EQ(cnt1.size() + 20, cnta1.size());
    CHECK_EQ(cnt2.size() + 30, cnta2.size());
    CHECK_EQ(cnt3.size() / 2, cnta3.size());

    for (size_t i = 0; i < cnt1.size(); ++i)
    {
        CHECK_EQ(cnt1[i], cnta1[i]);
    }
    for (size_t i = cnt1.size(); i < cnta1.size(); ++i)
    {
        CHECK_EQ('\0', cnta1[i]);
    }

    for (size_t i = 0; i < cnt2.size(); ++i)
    {
        CHECK_EQ(cnt2[i], cnta2[i]);
    }
    for (size_t i = cnt2.size(); i < cnta2.size(); ++i)
    {
        CHECK_EQ('\0', cnta2[i]);
    }

    for (size_t i = 0; i < cnta3.size(); ++i)
    {
        CHECK_EQ(cnt3[i], cnta3[i]);
    }
}

TEST_CASE("API: space" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

    check_lua_file(L, "test_space.lua");

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "space_1"));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "capacity"));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "free"));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "available"));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "space_2"));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "capacity"));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "free"));
    CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "available"));

    CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "space_invalid"));
    CHECK_EQ(false, stdfs::lua::tobool(L, lua_gettop(L)));
}

TEST_CASE("API: symlinks" * doctest::no_breaks(true))
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    REQUIRE_NE(L, nullptr);
    create_testcase_structure(".");

    check_lua_file(L, "test_symlinks.lua");

    fs::file_status const sym_stat_1 = fs::symlink_status("testcase/non-empty-folder/file-1.symlink");
    fs::file_status const sym_stat_2 = fs::symlink_status("testcase/non-empty-folder/folder-2.symlink");
    CHECK_EQ(fs::file_type::symlink, sym_stat_1.type());
    CHECK_EQ(fs::file_type::symlink, sym_stat_2.type());

    CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "symlink1"));
    check_lua_code(L, "symlink1_str = tostring(symlink1)");
    CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "symlink1_str"));
    char const* symlink1_str = luaL_checkstring(L, -1);
    CHECK_NE(nullptr, symlink1_str);
    CHECK_EQ(fs::path("testcase/non-empty-folder/file-1").string(), std::string(symlink1_str));

    CHECK_EQ(LUA_TUSERDATA, stdfs::lua::getglobal(L, "symlink2"));
    check_lua_code(L, "symlink2_str = tostring(symlink2)");
    CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "symlink2_str"));
    char const* symlink2_str = luaL_checkstring(L, -1);
    CHECK_NE(nullptr, symlink2_str);
    CHECK_EQ(fs::path("testcase/non-empty-folder/folder-2").string(), std::string(symlink2_str));

    auto const symlink_status_1 = fs::symlink_status("testcase/non-empty-folder/file-1.symlink");
    auto const symlink_status_2 = fs::symlink_status("testcase/non-empty-folder/folder-2.symlink");

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "symlink_status_1"));
    stdfs::lua::getfield(L, -1, "file_type");
    CHECK_EQ(static_cast<int>(symlink_status_1.type()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "symlink_status_1"));
    stdfs::lua::getfield(L, -1, "file_perms");
    CHECK_EQ(static_cast<int>(symlink_status_1.permissions()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "symlink_status_2"));
    stdfs::lua::getfield(L, -1, "file_type");
    CHECK_EQ(static_cast<int>(symlink_status_2.type()), lua_tointeger(L, -1));

    CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "symlink_status_2"));
    stdfs::lua::getfield(L, -1, "file_perms");
    CHECK_EQ(static_cast<int>(symlink_status_2.permissions()), lua_tointeger(L, -1));
}

TEST_CASE("API: write_time" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
	create_testcase_structure(".");

	check_lua_file(L, "test_write_time.lua");

	CHECK_EQ(LUA_TTABLE, stdfs::lua::getglobal(L, "wrtm1"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "sec"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "min"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "hour"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "mday"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "mon"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "year"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "wday"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "yday"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "isdst"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "since_epoch"));
	CHECK_EQ(LUA_TSTRING, stdfs::lua::getfield(L, 1, "ctm"));
	CHECK_EQ(LUA_TTABLE, stdfs::lua::getfield(L, 1, "loc"));

	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "sec"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "min"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "hour"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "mday"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "mon"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "year"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "wday"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "yday"));
	CHECK_EQ(LUA_TNUMBER, stdfs::lua::getfield(L, 1, "since_epoch"));
}

TEST_CASE("API: temp_dir, work_dir" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
    check_lua_code(L, R"(package.cpath = "./lib?.so;" .. package.cpath;fs = require("stdfs"))");

	SUBCASE("Temporary directory")
	{
		check_lua_code(L, "tmp = fs.temp_dir()");
		check_lua_code(L, "tmpStr = tostring(tmp)");
		CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "tmpStr"));
		char const* tempPath = luaL_checkstring(L, -1);
		CHECK_NE(nullptr, tempPath);
		CHECK_EQ(fs::temp_directory_path().string(), std::string(tempPath));
	}

	SUBCASE("Working directory get")
	{
		check_lua_code(L, "tmp = fs.work_dir()");
		check_lua_code(L, "tmpStr = tostring(tmp)");
		CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "tmpStr"));
		char const* tempPath = luaL_checkstring(L, -1);
		CHECK_NE(nullptr, tempPath);
		CHECK_EQ(fs::current_path().string(), std::string(tempPath));
	}

	SUBCASE("Working directory set")
	{
		fs::create_directory("testcase");
		check_lua_code(L, R"(fs.work_dir("testcase"))");
		check_lua_code(L, "tmp = fs.work_dir()");
		check_lua_code(L, "tmpStr = tostring(tmp)");
		CHECK_EQ(LUA_TSTRING, stdfs::lua::getglobal(L, "tmpStr"));
		char const* tempPath = luaL_checkstring(L, -1);
		CHECK_NE(nullptr, tempPath);
		CHECK_EQ(fs::current_path().string(), std::string(tempPath));
		fs::current_path("..");
	}
}

TEST_CASE("API unicode" * doctest::no_breaks(true))
{
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	REQUIRE_NE(L, nullptr);
    check_lua_code(L, R"(package.cpath = "./lib?.so;" .. package.cpath;fs = require("stdfs"))");

	REQUIRE_MESSAGE(create_testcase_structure("."), "Couldn't create testcase folder");

	SUBCASE("Functions create_dir, exists, remove")
	{
		for (int i = 0; i < 100; ++i)
		{
			auto randName = get_random_string();
			fs::path randPath(randName);
			while (fs::exists(randPath))
			{
				randName = get_random_string();
				randPath = s_dump_dir / randName;
			}
			auto rs = convert_from_utf8(randPath.u32string());

			auto existsCmd = std::string("tmp = fs.exists('") + rs + "')";
			check_lua_code(L, existsCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(false, stdfs::lua::tobool(L, -1) != 0);

			auto createCmd = std::string("tmp = fs.create_dir('") + rs + "')";
			check_lua_code(L, createCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			check_lua_code(L, existsCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			CHECK(fs::exists(randPath));

			auto removeCmd = std::string("tmp = fs.remove('") + rs + "')";
			check_lua_code(L, removeCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			check_lua_code(L, existsCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(false, stdfs::lua::tobool(L, -1) != 0);
			CHECK(!fs::exists(randPath));
		}
	}

	SUBCASE("Functions copy, rename, exists")
	{
		for (int i = 0; i < 100; ++i)
		{
			auto randName1 = get_random_string();
			auto randName2 = get_random_string();
			fs::path randPath1(randName1);
			fs::path randPath2(randName2);
			while (fs::exists(randPath1))
			{
				randName1 = get_random_string();
				randPath1 = s_dump_dir / randName1;
			}
			while (fs::exists(randPath2))
			{
				randName2 = get_random_string();
				randPath2 = s_dump_dir / randName2;
			}
			auto rs1 = convert_from_utf8(randPath1.u32string());
			auto rs2 = convert_from_utf8(randPath2.u32string());

			std::ofstream newFile(randPath1);
			newFile.close();

			auto existsCmd1 = std::string("tmp = fs.exists('") + rs1 + "')";
			auto existsCmd2 = std::string("tmp = fs.exists('") + rs2 + "')";
			check_lua_code(L, existsCmd1.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			check_lua_code(L, existsCmd2.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(false, stdfs::lua::tobool(L, -1) != 0);
			CHECK(fs::exists(randPath1));
			CHECK(!fs::exists(randPath2));

			auto copyCmd = std::string("tmp = fs.copy('") + rs1 + "', '" + rs2 + "')";
			check_lua_code(L, copyCmd.c_str());
			check_lua_code(L, existsCmd2.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			CHECK(fs::exists(randPath1));
			CHECK(fs::exists(randPath2));

			auto removeCmd = std::string("tmp = fs.remove('") + rs2 + "')";
			check_lua_code(L, removeCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			CHECK(!fs::exists(randPath2));

			auto moveCmd = std::string("tmp = fs.rename('") + rs1 + "', '" + rs2 + "')";
			check_lua_code(L, moveCmd.c_str());
			check_lua_code(L, existsCmd1.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(false, stdfs::lua::tobool(L, -1) != 0);
			check_lua_code(L, existsCmd2.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
			CHECK(!fs::exists(randPath1));
			CHECK(fs::exists(randPath2));

			check_lua_code(L, removeCmd.c_str());
			CHECK_EQ(LUA_TBOOLEAN, stdfs::lua::getglobal(L, "tmp"));
			CHECK_EQ(true, stdfs::lua::tobool(L, -1) != 0);
		}
	}
}

TEST_SUITE_END();
