#pragma once

#include <map>
#include <string>

extern std::map<std::string, int> s_file_sizes;

std::u32string  get_random_string(size_t length = 32);
std::u32string  convert_to_utf8(char const* b, char const* e);
std::string     convert_from_utf8(std::u32string const& s);

bool create_testcase_structure(std::string const& directory);
