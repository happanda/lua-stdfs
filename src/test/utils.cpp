#include "utils.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <codecvt>
#include <fstream>
#include <iostream>
#include <locale>
#include <filesystem>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace fs = std::filesystem;
using convert_type = std::codecvt_utf8<char32_t>;
static std::wstring_convert<convert_type, typename convert_type::intern_type> s_converter;

static fs::path s_dump_dir("testcase");

static std::vector<char32_t> prepare_utf8_symbols()
{
    static char32_t constexpr s_invalid_chars[] = { ' ', '!', '"', '#', '$', '%', '&', '\'', '*', '+',
                                                    '/', '(', ')', ':', '<', '=', '>', '?', '@', '\\', '{', '|', '}' };
    int constexpr last_code = 55296;

    std::vector<char32_t> symbols;
    symbols.reserve(last_code);
    for (char32_t i = 33; i < last_code; ++i)
    {
        if ((i < 127 || i > 160) && std::find(std::begin(s_invalid_chars), std::end(s_invalid_chars), i) == std::end(s_invalid_chars))
        {
            symbols.push_back(i);
        }
    }
    return symbols;
}

std::vector<char32_t> const s_symbols = prepare_utf8_symbols();

std::u32string get_random_string(size_t length)
{
    std::u32string str(length, '-');
    for (size_t i = 0; i < length; ++i)
    {
        str[i] = s_symbols[std::rand() % s_symbols.size()];
    }
    return str;
}

std::string get_random_bytes(size_t length = 32)
{
    std::string str(length, '-');
    int const max_byte = std::pow(2, sizeof(char));
    for (size_t i = 0; i < length; ++i)
    {
        str[i] = (std::rand() % max_byte);
    }
    return str;
}

std::u32string convert_to_utf8(char const* b, char const* e)
{
    try
    {
        return s_converter.from_bytes(b, e);
    }
    catch (std::range_error const& re)
    {
        return std::u32string();
    }
}

std::string convert_from_utf8(std::u32string const& s)
{
    try
    {
        return s_converter.to_bytes(s);
    }
    catch (std::range_error const& re)
    {
        return std::string();
    }
}

std::set<fs::path> create_rand_files(fs::path const& inPath, int maxFiles, int depth)
{
    std::set<fs::path> createdPaths;

    for (int i = 0; i < maxFiles; ++i)
    {
        auto randName = get_random_string(8);
        fs::path randPath = inPath / fs::path(randName);
        while (fs::exists(randPath))
        {
            randName = get_random_string(8);
            randPath = inPath / randName;
        }
        if (std::rand() % 100 < 50)
        {
            std::ofstream newFile(randPath);
            newFile.close();
            createdPaths.insert(randPath);
        }
        else
        {
            fs::create_directories(randPath);
            createdPaths.insert(randPath);
            if (depth > 0)
            {
                auto subfolder = create_rand_files(randPath, maxFiles, depth - 1);
                createdPaths.insert(subfolder.begin(), subfolder.end());
            }
        }
    }
    return createdPaths;
}

static int const s_text_len = 80;
static int const s_bin_len = 400;
std::map<std::string, int> s_file_sizes;

void create_file_empty(fs::path const& path)
{
    std::ofstream file(path);
    file.close();
    s_file_sizes[path.lexically_normal()] = 0;
}

void create_file_text(fs::path const& path)
{
    std::ofstream file(path, std::ios::out);
    auto const str = convert_from_utf8(get_random_string(s_text_len));
    file << str;
    file.close();
    s_file_sizes[path.lexically_normal()] = str.size() * sizeof(char);
}

void create_file_binary(fs::path const& path)
{
    // TODO: generate random bytes
    std::ofstream file(path, std::ios::binary);
    auto const str = get_random_bytes(s_bin_len);
    file.write(str.data(), str.size());
    file.close();
    s_file_sizes[path.lexically_normal()] = str.size() * sizeof(char);
}

static std::array<char const*, 10> testcase_directories =
        {
                "testcase/",
                "testcase/empty-folder/",
                "testcase/non-empty-folder/",
                "testcase/non-empty-folder/folder-1/",
                "testcase/non-empty-folder/folder-2/",
                "testcase/folder-with-folders/",
                "testcase/folder-with-folders/folder-1/",
                "testcase/folder-with-folders/folder-2/",
                "testcase/folder-with-folders/folder-2/folder-2-1/",
                "testcase/folder-with-folders/folder-2/folder-2-2/",
        };
static std::array<char const*, 6> testcase_files =
        {
                "testcase/non-empty-folder/file-1",
                "testcase/non-empty-folder/file-2",
                "testcase/non-empty-folder/file-3",
                "testcase/non-empty-folder/folder-2/file-1",
                "testcase/non-empty-folder/folder-2/file-2",
                "testcase/non-empty-folder/folder-2/file-3",
        };
//"testcase/folder-with-symlink/"
//"testcase/folder-with-hardlink/"
//"testcase/empty-folder", "testcase/folder-with-symlink/symlink-1/"
//"testcase/non-empty-folder", "testcase/folder-with-symlink/symlink-2/"

bool create_testcase_structure(std::string const& directory)
{
    try
    {
        fs::path fsdir{ directory };
        fs::remove_all(fsdir / "testcase");
        for (auto const& path : testcase_directories)
        {
            fs::create_directory(fsdir / path);
        }
        create_file_empty(fsdir / testcase_files[0]);
        create_file_empty(fsdir / testcase_files[3]);
        create_file_text(fsdir / testcase_files[1]);
        create_file_text(fsdir / testcase_files[4]);
        create_file_binary(fsdir / testcase_files[2]);
        create_file_binary(fsdir / testcase_files[5]);
    }
    catch (fs::filesystem_error const& fe)
    {
        std::cerr << "Filesystem error while preparing testcase: " << fe.what();
        return false;
    }
    return true;
}