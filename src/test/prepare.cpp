#include "utils.h"

#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;


int main(int argc, char* argv[]) {
    if (argc <= 1) {
        std::cout << "Usage: prepare <target_directory>" << std::endl
            << "This will create test structure inside <target_directory>/testcase" << std::endl;
        return 1;
    }
    fs::path target_dir(argv[1]);
    if (!create_testcase_structure(target_dir)) {
        return 2;
    }
    return 0;
}