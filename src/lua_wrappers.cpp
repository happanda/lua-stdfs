#include "common.h"
#include "lua_wrappers.h"

int stdfs::lua::pushbool(lua_State* L, bool value) {
    lua_pushboolean(L, value ? 1 : 0);
    return 1;
}

bool stdfs::lua::tobool(lua_State* L, int index) {
    int const val = lua_toboolean(L, index);
    return val != 0;
}
