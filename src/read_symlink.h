#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Pushes on stack target of a symbolic link
     * @return Result of \p push_path() in cases of success
     * @return Result of \p luaL_error() in case of error
     */
    int read_symlink(lua_State* L);
} // namespace stdfs
