#pragma once

struct lua_State;

namespace stdfs
{
    /**
     * @brief Checks if path exists
     * @return \a 1 in case of successful check, as one boolean value is pushed on stack - whether path exists or not
     * @return Result of \a luaL_error() in case of error
     */
    int exists(lua_State* L);
} // namespace stdfs
