fs = require("stdfs")

local rockspec_template =
[=[rockspec_format = "3.0"
package = "stdfs"
version = "_VERSION_"
source = {
    url = "git+https://gitlab.com/happanda/lua-stdfs.git",
    tag = "v_VERSION_",
}
description = {
    labels = { "filesystem", "cpp" },
    summary = "Filesystem API from C++ std::filesystem.",
    detailed = [[
stdfs is a filesytem API library. It's made as a thin wrapper around C++ std::filesystem.
With the following design goals:
- UTF-8 paths support
- easy implementing
- wide range of API tools
- no reinventing the wheel
]],
    homepage = "https://happanda.gitlab.io/stdfs-webpage/",
    license = "MIT",
}
dependencies = {
    "lua >= 5.1",
    "luarocks-build-extended",
}
build = {
    type = "extended",
    platforms = {
        windows = {
            modules = {
                stdfs = {
                    variables = {
                        CXXFLAG_EXTRAS = { "-std:c++17" }
                    }
                }
            }
        },
        unix = {
            modules = {
                stdfs = {
                    variables = {
                        CXXFLAG_EXTRAS = { "-std=c++17", "-fPIC", "-Wextra" },
                        LIBFLAG_EXTRAS = { "-lstdc++fs" },
                    }
                }
            }
        }
    },
    modules = {
        stdfs = {
            variables = {
                CXXFLAG_EXTRAS = { "-O2" },
            },
            incdirs = {
                "src"
            },
            sources = {
_SOURCES_
            }
        }
    }
}]=]

local exclude_cpp = { ["debug.cpp"] = true }


local parse_version = function(file_path)
    local version = nil
    for line in io.lines(file_path) do
        for ver in string.gmatch(line, 'set_version%(".+"%)') do
            for maj, min, patch in string.gmatch(ver, "(%d)%.(%d)%.(%d)") do
                version = {}
                version["major"] = maj
                version["minor"] = min
                version["patch"] = patch
                break
            end
            if version ~= nil then
                break
            end
        end
        if version ~= nil then
            break
        end
    end
    return version
end


local ver_str = ""

if arg[1] ~= nil then
    ver_str = arg[1]
else
    local version = parse_version("../xmake.lua")
    if version == nil then
        error("Couldn't parse version from ../xmake.lua")
        os.exit(false)
    end

    ver_str = version["major"] .. "." .. version["minor"] .. "-" .. version["patch"]
end

local sources = {}
for p in fs.ls("../src") do
    if ".cpp" == tostring(p.ext) and exclude_cpp[tostring(p.filename)] == nil then
        table.insert(sources, '                "src/' .. p.filename .. '",')
    end
end
table.sort(sources)

local sources_str = ""
for k, v in pairs(sources) do
    if k > 1 then
        sources_str = sources_str .. '\n'
    end
    sources_str = sources_str .. v
end

if ver_str == "scm-0" then
    rockspec_template = rockspec_template:gsub('    tag = "v_VERSION_",\n', '')
end
rockspec_template = rockspec_template:gsub('_VERSION_', ver_str)
rockspec_template = rockspec_template:gsub('_SOURCES_', sources_str)

local out_file_name = "stdfs-" .. ver_str .. ".rockspec"
local out_file = io.open(out_file_name, "w")
if out_file == nil then
    error("Couldn't open output file for write: " .. out_file_name)
    os.exit(false)
end

out_file:write(rockspec_template)
out_file:close()
