fs = require("stdfs")

fs.rename("testcase/non-empty-folder/file-3", "testcase/non-empty-folder/renamed-file-3")
fs.rename("testcase/non-empty-folder/file-1", "testcase/non-empty-folder/file-2")

fs.rename("testcase/non-empty-folder", "testcase/non-empty-folder")
fs.rename("testcase/empty-folder", "testcase/renamed-empty-folder")

fs.rename("testcase/folder-with-folders", "testcase/renamed-folder-with-folders")

rename_invalid = pcall(function() fs.rename("testcase/doesnt-exist", "testcase/renamed-doesnt-exist") end)
