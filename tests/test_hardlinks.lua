fs = require("stdfs")

local fp1 = fs.path("testcase/non-empty-folder/file-2")
local fp2 = fs.path("testcase/non-empty-folder/file-2.hardlink1")
local fp3 = fs.path("testcase/non-empty-folder/file-2.hardlink2")

link_count01 = fs.hardlink_count(fp1)

fs.create_hardlink(fp1, fp2)
link_count11 = fs.hardlink_count(fp1)
link_count12 = fs.hardlink_count(fp2)

fs.create_hardlink(fp1, fp3)
link_count21 = fs.hardlink_count(fp1)
link_count22 = fs.hardlink_count(fp2)
link_count23 = fs.hardlink_count(fp3)

fs.remove(fp1)
link_count32 = fs.hardlink_count(fp2)
link_count33 = fs.hardlink_count(fp3)

fs.remove(fp2)
link_count43 = fs.hardlink_count(fp3)
