fs = require("stdfs")

removed_1 = fs.remove("testcase/empty-folder")
removed_2 = fs.remove("testcase/non-empty-folder/file-1")
removed_3 = pcall(function() fs.remove("testcase/non-empty-folder") end)

removed_4 = fs.remove("testcase/doesnt-exist")