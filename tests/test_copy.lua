fs = require('stdfs')

cperr1 = pcall(function() fs.copy('testcase/doesnt-exist', 'testcase/any') end)
cperr2 = pcall(function() fs.copy('testcase/non-empty-folder/file-1', 'testcase/non-empty-folder/file-1') end)
cperr3 = pcall(function() fs.copy('testcase/non-empty-folder', 'testcase/non-empty-folder') end)
cperr4 = pcall(function() fs.copy('testcase/empty-folder', 'testcase/non-empty-folder/file-1') end)
cperr5 = pcall(function() fs.copy('testcase/non-empty-folder/file-2', 'testcase/non-empty-folder/file-3') end)

fs.copy('testcase/non-empty-folder/file-1', 'testcase/empty-folder')
fs.copy('testcase/non-empty-folder/file-2', 'testcase/copy-file-2')

fs.copy('testcase/non-empty-folder', 'testcase/copy-non-empty-folder', fs.copy_opts.recursive)
