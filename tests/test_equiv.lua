fs = require("stdfs")

self_equiv_1 = fs.equiv("testcase/non-empty-folder", "./testcase/non-empty-folder")
self_equiv_2 = fs.equiv("testcase/non-empty-folder/", "testcase/non-empty-folder/../non-empty-folder")
self_equiv_3 = fs.equiv("testcase/non-empty-folder/file-1", "./testcase/../testcase/non-empty-folder/file-1")

self_unequiv_1 = fs.equiv(".", "..")
self_unequiv_2 = fs.equiv("testcase/non-empty-folder/folder-1", "testcase/non-empty-folder/folder-2")
self_unequiv_3 = fs.equiv("testcase/non-empty-folder/folder-2/file-1", "testcase/non-empty-folder/folder-2/file-2")

invalid_unequiv_1 = pcall(function() fs.equiv("testcase/file-doesnt-exist-1", "testcase/file-doesnt-exist-2") end)
