-- fix the issue that Lua doesn't see "libLIBRARY.so" modules
-- also, put current directory in front so we test what's just built
package.cpath = "./lib?.so;" .. package.cpath