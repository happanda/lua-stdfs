fs = require("stdfs")

exists_1 = fs.exists("testcase/non-empty-folder")
exists_2 = fs.exists("testcase/non-empty-folder/file-1")
exists_3 = fs.exists("testcase/non-empty-folder/file-2")
exists_4 = fs.exists("testcase/non-empty-folder/folder-1")
exists_5 = fs.exists("testcase/doesnt-exist")
exists_6 = fs.exists(".")
exists_7 = fs.exists("..")