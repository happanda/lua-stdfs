fs = require("stdfs")

removed_1 = fs.remove_all("testcase/empty-folder")
removed_2 = fs.remove_all("testcase/non-empty-folder/file-1")
removed_3 = fs.remove_all("testcase/non-empty-folder")
removed_4 = fs.remove_all("testcase/doesnt-exist")