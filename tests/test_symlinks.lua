fs = require("stdfs")

fs.create_symlink("testcase/non-empty-folder/file-1", "testcase/non-empty-folder/file-1.symlink")
fs.create_symlink("testcase/non-empty-folder/folder-2", "testcase/non-empty-folder/folder-2.symlink")

symlink1 = fs.read_symlink("testcase/non-empty-folder/file-1.symlink")
symlink2 = fs.read_symlink("testcase/non-empty-folder/folder-2.symlink")

symlink_status_1 = fs.symlink_status("testcase/non-empty-folder/file-1.symlink")
symlink_status_2 = fs.symlink_status("testcase/non-empty-folder/folder-2.symlink")
