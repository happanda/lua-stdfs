fs = require("stdfs")

folder_is = {
    fs.is_block_file("testcase/empty-folder"),
    fs.is_character_file("testcase/empty-folder"),
    fs.is_directory("testcase/empty-folder"),
    fs.is_empty("testcase/empty-folder"),
    fs.is_fifo("testcase/empty-folder"),
    fs.is_other("testcase/empty-folder"),
    fs.is_regular_file("testcase/empty-folder"),
    fs.is_socket("testcase/empty-folder"),
    fs.is_symlink("testcase/empty-folder"),
}

file_is = {
    fs.is_block_file("testcase/non-empty-folder/file-1"),
    fs.is_character_file("testcase/non-empty-folder/file-1"),
    fs.is_directory("testcase/non-empty-folder/file-1"),
    fs.is_empty("testcase/non-empty-folder/file-1"),
    fs.is_fifo("testcase/non-empty-folder/file-1"),
    fs.is_other("testcase/non-empty-folder/file-1"),
    fs.is_regular_file("testcase/non-empty-folder/file-1"),
    fs.is_socket("testcase/non-empty-folder/file-1"),
    fs.is_symlink("testcase/non-empty-folder/file-1"),
}

file_type_invalid = pcall(function() fs.is_regular_file("testcase/file-doesnt-exist") end)