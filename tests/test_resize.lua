fs = require('stdfs')

rserr1 = pcall(function() fs.resize(".") end)
rserr2 = pcall(function() fs.resize("testcase/non-empty-folder", 12) end)
rserr3 = false
if _VERSION ~= "Lua 5.1" and _VERSION ~= "Lua 5.2" then
    -- in Lua 5.1 and 5.2 there is no proper good way to check for a number being an Integer
    -- this can be done by comparing abs(num - trunc(num)) to small epsilon
    -- but I don't feel it makes sense to go down this road
    -- so this test is not valid for Lua 5.1 and Lua 5.2
    rserr3 = pcall(function() fs.resize("testcase/non-empty-folder/file-1", 12.12) end)
end
rserr4 = pcall(function() fs.resize("testcase/non-empty-folder/file-1", -1) end)

print(rserr1, rserr2, rserr3, rserr4)
local fs1 = fs.file_size('testcase/non-empty-folder/file-1')
local fs2 = fs.file_size('testcase/non-empty-folder/file-2')
local fs3 = fs.file_size('testcase/non-empty-folder/file-3')

fs.resize('testcase/non-empty-folder/file-1', fs1 + 20)
fs.resize('testcase/non-empty-folder/file-2', fs2 + 30)
fs.resize('testcase/non-empty-folder/file-3', math.floor(fs3 / 2))
