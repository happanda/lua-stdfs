fs = require("stdfs")

new_folder = pcall(function() fs.create_dir("testcase/create_dir-1") end)

new_folder_subfolders = pcall(function() fs.create_dir("testcase/create_dir-2/create_dir-2-1") end)

existing_folder = pcall(function() fs.create_dir("testcase/empty-folder") end)

existing_file = pcall(function() fs.create_dir("testcase/non-empty-folder/file-1") end)
