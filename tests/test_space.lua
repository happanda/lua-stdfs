fs = require("stdfs")

space_1 = fs.space("testcase/non-empty-folder/file-1")
space_2 = fs.space("testcase/non-empty-folder")

space_invalid = pcall(function() fs.space("testcase/file-doesnt-exist") end)
