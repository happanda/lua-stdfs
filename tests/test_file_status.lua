fs = require("stdfs")

status_1 = fs.file_status("testcase/non-empty-folder")
status_2 = fs.file_status("testcase/non-empty-folder/file-1")
status_3 = fs.file_status("testcase/file-doesnt-exist")

status_invalid = pcall(function() fs.file_status("testcase/file-doesnt-exist") end)