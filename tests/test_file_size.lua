fs = require("stdfs")

size_1 = fs.file_size("testcase/non-empty-folder/file-1")
size_2 = fs.file_size("testcase/non-empty-folder/file-2")
size_3 = fs.file_size("testcase/non-empty-folder/file-3")

size_invalid = pcall(function() fs.file_size("testcase/file-doesnt-exist") end)
