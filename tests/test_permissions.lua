fs = require("stdfs")

local pf1 = fs.path("testcase/non-empty-folder/file-1")
local pf2 = fs.path("testcase/non-empty-folder/file-2")
local pf3 = fs.path("testcase/empty-folder")

fs.permissions(pf1, fs.file_perms.all, fs.perm_options.replace)
fs.permissions(pf2, fs.file_perms.none, fs.perm_options.replace)
fs.permissions(pf2, fs.file_perms.owner_read | fs.file_perms.owner_write, fs.perm_options.add)

fs.permissions(pf3, fs.file_perms.group_exec | fs.file_perms.others_exec, fs.perm_options.remove)
